<?php

use App\Http\Controllers\assetController;
use App\Http\Controllers\Controller;
use App\Http\Controllers\MaintenanceActiveController;
use App\Http\Controllers\MaintenanceHistoryController;
use App\Http\Controllers\DataUnitController;
use App\Http\Controllers\HomeController;
use App\Http\Controllers\MaintenanceController;
use App\Http\Controllers\MessageController;
use App\Http\Controllers\MessageMaintenanceController;
use App\Http\Controllers\MessageOperationController;
use App\Http\Controllers\MessagePerbaikanController;
use App\Http\Controllers\OperationActiveController;
use App\Http\Controllers\OperationsController;
use App\Http\Controllers\OperationsHistoryControllers;
use App\Http\Controllers\ReportMaintenanceController;
use App\Http\Controllers\ReportOperationController;
use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider and all of them will
| be assigned to the "web" middleware group. Make something great!
|
*/

//route Auth
Route::get('/login', [Controller::class, 'loginShow'])->name('login');
Route::post('/login/aksi', [Controller::class, 'loginAction'])->name('login.action');


Route::middleware(['auth'])->group(function () {

    Route::get('/', [HomeController::class, 'index'])->name('home');
    Route::get('/get-realtime-data', [HomeController::class, 'getRealtimeData']);

    //route operation
    Route::get('operations', [OperationsController::class, 'index'])->name('index.operations');
    Route::get('operations-create-data', [OperationsController::class, 'create'])->name('create.operations');
    Route::post('operations-store-data', [OperationsController::class, 'store'])->name('store.operations');

    Route::get('revisi-operation', [OperationsController::class, 'revisi_page'])->name('revisiPage.operation');
    Route::get('revisi-operation/{id}', [OperationsController::class, 'revisiOperation'])->name('revisiOperation');
    Route::post('revisi-action/{id}/{param}', [OperationsController::class, 'revisiAction'])->name('revisiAction');

    // report operation
    Route::get('report-operation', [ReportOperationController::class, 'index'])->name('report-operation');
    Route::get('downloadReport-operation', [ReportOperationController::class, 'downloadReport'])->name('downloadReport-operation');

    //route asset
    Route::get('data-unit', [assetController::class, 'index'])->name('index.asset');
    Route::get('data-unit-create-data', [assetController::class, 'create'])->name('create.asset');
    Route::post('data-unit-store-data', [assetController::class, 'store'])->name('store.asset');
    Route::get('/action', [assetController::class, 'action'])->name('action');

    Route::get('logout', [Controller::class, 'logout'])->name('logout');
    Route::get('Profil', [Controller::class, 'profilView'])->name('profilView');

    // maintenance
    Route::get('/maintenance', [MaintenanceController::class, 'index'])->name('maintenance');
    Route::get('/add-maintenance', [MaintenanceController::class, 'page_add'])->name('add-maintenance');
    Route::post('/add-maintenance-record', [MaintenanceController::class, 'add_maintenance'])->name('add-maintenance-record');

    Route::get('revisi-maintenance', [MaintenanceController::class, 'revisi_page'])->name('revisiPage.maintenance');
    Route::get('revisi-maintenance/{id}', [MaintenanceController::class, 'revisiMaintenance'])->name('revisiMaintenance.page');
    Route::post('revisi-action-maintenance/{id}/{param}', [MaintenanceController::class, 'revisiAction'])->name('revisiMaintenance.action');

    // report-maintenance
    Route::get('/report-maintenance', [ReportMaintenanceController::class, 'index'])->name('report-maintenance');
    Route::get('/downloadReport', [ReportMaintenanceController::class, 'downloadReport'])->name('downloadReport');

    // operation
    Route::get('edit-operation/{id}', [OperationsController::class, 'page_edit'])->name('pageEditOperation');
    Route::post('action-edit-operation/{id}', [OperationsController::class, 'edit'])->name('edit.operation');
    Route::post('message-operation/{id}', [MessageOperationController::class, 'message'])->name('messageOperation');
    Route::get('delete-operation/{id}', [OperationsController::class, 'delete'])->name('delete-operation');

    // maintenance
    Route::get('edit-maintenande/{id}', [MaintenanceController::class, 'page_edit'])->name('pageEditMaintenance');
    Route::post('action-edit-maintenance/{id}', [MaintenanceController::class, 'edit'])->name('editMaintenance');
    Route::post('message-maintenance/{id}', [MessageMaintenanceController::class, 'message'])->name('messageMaintenance');
    Route::get('delete-maintenance/{id}', [MaintenanceController::class, 'delete'])->name('delete-maintenance');

    // Asset
    Route::get('edit-asset/{id}', [assetController::class, 'edit_page'])->name('editPage');
    Route::post('edit-asset-action/{id}', [assetController::class, 'edit'])->name('editAsset');
    // Route::get('deleted-asset/{id}', [assetController::class, 'deleted'])->name('delete-asset');
    Route::get('delete-asset/{id}', [assetController::class, 'confirmDelete'])->name('delete-asset');
});

Route::middleware(['role:supervisor'])->group(function () {
});
