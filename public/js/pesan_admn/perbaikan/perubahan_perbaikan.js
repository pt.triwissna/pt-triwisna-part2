function fetchAndDisplayMaintenanceMessages() {
    fetch('/get-maintenance-messages')
        .then(response => response.json())
        .then(data => {
            const tableContainer = document.getElementById('maintenance-messages-table');
            const table = createTable(data);
            tableContainer.innerHTML = '';
            tableContainer.appendChild(table);
        })
        .catch(error => {
            console.error('Gagal mengambil data:', error);
        });
}

function createTable(data) {
    const table = document.createElement('table');
    table.className = 'table table-striped table-hover';

    const thead = document.createElement('thead');
    const headerRow = document.createElement('tr');
    headerRow.innerHTML = `
        <th>Tanggal</th>
        <th>Title</th>
        <th>Contents</th>
        <th>Action</th>
    `;
    thead.appendChild(headerRow);
    table.appendChild(thead);

    const tbody = document.createElement('tbody');
    const userRole = loggedInUser.role.role; // Ambil peran pengguna yang sedang login

    data.forEach(item => {
        // Filter data sesuai dengan peran pengguna yang sedang login
        if ((userRole === 'admin' && item.for_to === 'admin') ||
            (userRole === 'supervisor' && item.for_to === 'supervisor')) {

            const row = document.createElement('tr');

            // Menambahkan kolom untuk tanggal pesan
            const dateCell = document.createElement('td');
            dateCell.textContent = item.date_message;
            row.appendChild(dateCell);

            // Menambahkan kolom untuk judul pesan
            const titleCell = document.createElement('td');
            titleCell.textContent = item.title_message;
            row.appendChild(titleCell);

            // Menambahkan kolom untuk isi pesan
            const contentsCell = document.createElement('td');
            contentsCell.textContent = item.contents_message;
            row.appendChild(contentsCell);

            // Menambahkan kolom untuk tautan "Perbaikan"
            const editCell = document.createElement('td');
            const editLink = document.createElement('a');

            // Menggunakan URL dengan parameter id secara langsung
            editLink.href = `/edit-maintenance/show/${item.id_maintenance_message}`;

            editLink.textContent = 'Perbaikan';
            editLink.className = 'custom-button';
            editLink.style = 'display: flex; width: 100px; height: 28px; flex-direction: column; justify-content: center; flex-shrink: 0; color: #FFF; text-align: center; font-family: Inter; font-size: 16px; font-style: normal; font-weight: 400; line-height: normal; background-color: #007BFF; border: none; border-radius: 5px; cursor: pointer; text-decoration: none; padding: 5px 10px;';
            
            editCell.appendChild(editLink);
            row.appendChild(editCell);

            tbody.appendChild(row);
        }
    });

    table.appendChild(tbody);

    return table;
}

// Panggil fungsi fetchAndDisplayMaintenanceMessages untuk pertama kali
fetchAndDisplayMaintenanceMessages();

// Atur interval untuk memperbarui data setiap beberapa detik
setInterval(fetchAndDisplayMaintenanceMessages, 3000);
