function updateRealtimeData() {
    const xhr = new XMLHttpRequest();
    xhr.open('GET', '/get-realtime-data', true);
    xhr.onreadystatechange = function () {
        if (xhr.readyState === XMLHttpRequest.DONE && xhr.status === 200) {
            const data = JSON.parse(xhr.responseText);
            const realtimeDataDiv = document.getElementById('realtime-data');
            realtimeDataDiv.innerHTML = `
                <div class="col-lg-6 col-md-12 col-6 mb-4">
                    <div class="card">
                        <div class="card-body text-center">
                            <span class="fw-semibold d-block mb-1">Total Peminjaman</span>
                            <h3 class="card-title mb-2 text-success">${data.total_peminjaman}</h3>
                        </div>
                    </div>
                </div>
                <div class="col-lg-6 col-md-12 col-6 mb-4">
                    <div class="card">
                        <div class="card-body text-center">
                            <span class="fw-semibold d-block mb-1">Total Perbaikan</span>
                            <h3 class="card-title mb-2 text-success">${data.total_perbaikan}</h3>
                        </div>
                    </div>
                </div>
            `;
        }
    };
    xhr.send();
}

