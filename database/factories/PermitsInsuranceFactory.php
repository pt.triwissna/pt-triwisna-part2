<?php

namespace Database\Factories;

use App\Models\PermitsInsuranceModels;
use Illuminate\Database\Eloquent\Factories\Factory;

/**
 * @extends \Illuminate\Database\Eloquent\Factories\Factory<\App\Models\PermitsInsuranceModels>
 */
class PermitsInsuranceFactory extends Factory
{
    /**
     * Define the model's default state.
     *
     * @return array<string, mixed>
     */

     protected $model = PermitsInsuranceModels::class;

    public function definition(): array
    {
        return [
            'stnk_issued' => $this->faker->date,
            'stnk_expired' => $this->faker->date,
            'kir_issued' => $this->faker->date,
            'kir_expired' => $this->faker->date,
            'insurance_issued' => $this->faker->date,
            'insurance_expired' => $this->faker->date,
            'record_adm_id' => 1,
        ];
    }
}
