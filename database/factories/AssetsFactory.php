<?php

namespace Database\Factories;

use App\Models\AssetsModel;
use App\Models\PermitsInsuranceModels;
use Illuminate\Database\Eloquent\Factories\Factory;

/**
 * @extends \Illuminate\Database\Eloquent\Factories\Factory<\App\Models\AssetsModel>
 */
class AssetsFactory extends Factory
{
    /**
     * Define the model's default state.
     *
     * @return array<string, mixed>
     */

     protected $model = AssetsModel::class;

public function definition(): array
{
    return [
        'id_type' => 1, // Ganti dengan ID tipe yang sesuai
        'id_ctgr' => 1, // Ganti dengan ID kategori yang sesuai
        'id_permits_insurance' => PermitsInsuranceModels::factory()->create(), // Gunakan create() di sini
        'manufacture' => $this->faker->company,
        'model' => $this->faker->word,
        'yom' => $this->faker->year,
        'no_unit' => $this->faker->unique()->numberBetween(1000, 9999),
        'flg_status' => 'T',
    ];
}

}
