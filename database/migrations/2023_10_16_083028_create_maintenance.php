<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

return new class extends Migration
{
    /**
     * Run the migrations.
     */
    public function up(): void
    {
        Schema::create('maintenance', function (Blueprint $table) {
            $table->id('maintenance_id');
            $table->integer('record_id');
            $table->foreignId('asset_id');
            $table->date('s_breakdown_date');
            $table->time('s_breakdown_time');
            $table->date('f_breakdown_date');
            $table->time('f_breakdown_time');
            $table->string('issue');
            $table->string('perform_by');
            $table->float('finance', 25, 2);
            $table->enum('flg', ['N', 'R', 'D']);
            $table->timestamps();

            // $table->foreign('asset_id')->references('asset_id')->on('asset')->onDelete('cascade');
        });
    }

    /**
     * Reverse the migrations.
     */
    public function down(): void
    {
        Schema::dropIfExists('maintenance');
    }
};
