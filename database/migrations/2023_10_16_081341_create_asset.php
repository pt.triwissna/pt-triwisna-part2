<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

return new class extends Migration
{
    /**
     * Run the migrations.
     */
    public function up(): void
    {
        Schema::create('asset', function (Blueprint $table) {
            $table->id('asset_id');
            $table->unsignedBigInteger('record_id');  
            $table->foreignId('type_asset_id');
            $table->foreignId('ctgr_asset_id');
            $table->string('manufactur');
            $table->string('model');
            $table->integer('yom');
            $table->string('no_unit')->unique();
            $table->foreignId('permit_insurance_id')->nullable();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     */
    public function down(): void
    {
        Schema::dropIfExists('asset');
    }
};
