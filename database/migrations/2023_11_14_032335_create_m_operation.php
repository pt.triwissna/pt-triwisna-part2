<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

return new class extends Migration
{
    /**
     * Run the migrations.
     */
    public function up(): void
    {
        Schema::create('m_operation', function (Blueprint $table) {
            $table->id('m_operation_id');
            $table->date('date');
            $table->text('message');
            $table->enum('by_role', ['S', 'A']);  // "S" artinya Supervisor && "A" artinya admin
            $table->integer('operation_id');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     */
    public function down(): void
    {
        Schema::dropIfExists('m_operation');
    }
};
