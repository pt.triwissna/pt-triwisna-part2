<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

return new class extends Migration
{
    /**
     * Run the migrations.
     */
    public function up(): void
    {
        Schema::create('permit_insurance', function (Blueprint $table) {
            $table->id('permit_insurance_id');
            $table->integer('stnk_issued')->nullable();
            $table->integer('stnk_expired')->nullable();
            $table->integer('kir_issued')->nullable();
            $table->integer('kir_expired')->nullable();
            $table->foreignId('insurance_type_id')->nullable();
            $table->integer('insurance_issued')->nullable();
            $table->integer('insurance_expired')->nullable();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     */
    public function down(): void
    {
        Schema::dropIfExists('permit_insurance');
    }
};
