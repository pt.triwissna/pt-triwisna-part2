<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

return new class extends Migration
{
    /**
     * Run the migrations.
     */
    public function up(): void
    {
        Schema::create('operation', function (Blueprint $table) {
            $table->id('operation_id');
            $table->integer('record_id');
            $table->foreignId('asset_id');
            $table->date('date');
            $table->string('shift');
            $table->integer('s_hourmeter')->nullable();
            $table->integer('f_hourmeter')->nullable();
            $table->time('s_worktime');
            $table->time('f_worktime');
            $table->time('s_otherdelay')->nullable();
            $table->time('f_otherdelay')->nullable();
            $table->integer('s_odometer')->nullable();
            $table->integer('f_odometer')->nullable();
            $table->integer('fuel');
            $table->text('project');
            $table->string('operator');
            $table->integer('trip');
            $table->enum('flg', ['N', 'R', 'D']);
            $table->timestamps();


            // $table->foreign('asset_id')->references('asset_id')->on('asset')->onDelete('cascade');
        });
    }

    /**
     * Reverse the migrations.
     */
    public function down(): void
    {
        Schema::dropIfExists('operation');
    }
};
