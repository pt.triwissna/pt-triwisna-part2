<?php

namespace Database\Seeders;

use Illuminate\Database\Console\Seeds\WithoutModelEvents;
use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;

class CategoryAssetSeed extends Seeder
{
    /**
     * Run the database seeds.
     */
    public function run(): void
    {
        DB::table('ctgr_asset')->insert([
            'ctgr_name' => 'PRODUCTION UNIT'
        ]);
        DB::table('ctgr_asset')->insert([
            'ctgr_name' => 'SUPPORTING UNIT'
        ]);
    }
}
