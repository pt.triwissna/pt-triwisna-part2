<?php

namespace Database\Seeders;

use Illuminate\Database\Console\Seeds\WithoutModelEvents;
use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Hash;

class UserSeed extends Seeder
{
    /**
     * Run the database seeds.
     */
    public function run(): void
    {
        DB::table('users')->insert([
            'name' => 'admin',
            'username' => 'admin',
            'role_id' => '1',
            'password' => Hash::make('password'),
        ]);

        DB::table('users')->insert([
            'name' => 'supervisor',
            'username' => 'supervisor',
            'role_id' => '2',
            'password' => Hash::make('password'),
        ]);

        DB::table('users')->insert([
            'name' => 'manager',
            'username' => 'manager',
            'role_id' => '3',
            'password' => Hash::make('password'),
        ]);

        //untuk roles
        DB::table('role')->insert([
            'role' => 'admin',
        ]);
        DB::table('role')->insert([
            'role' => 'supervisor',
        ]);
        DB::table('role')->insert([
            'role' => 'manager',
        ]);
    }
}
