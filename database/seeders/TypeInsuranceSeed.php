<?php

namespace Database\Seeders;

use Illuminate\Database\Console\Seeds\WithoutModelEvents;
use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;

class TypeInsuranceSeed extends Seeder
{
    /**
     * Run the database seeds.
     */
    public function run(): void
    {
        DB::table('insurance_type')->insert([
            'insurance_type_name' => 'All RISK'
        ]);
        DB::table('insurance_type')->insert([
            'insurance_type_name' => 'TLO'
        ]);
        DB::table('insurance_type')->insert([
            'insurance_type_name' => 'Comprehensive'
        ]);
    }
}
