<?php

namespace Database\Seeders;

use Illuminate\Database\Console\Seeds\WithoutModelEvents;
use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;

class TypeAssetSeed extends Seeder
{
    /**
     * Run the database seeds.
     */
    public function run(): void
    {
        DB::table('type_asset')->insert([
            'type_name' => 'EXCAVATOR'
        ]);
        DB::table('type_asset')->insert([
            'type_name' => 'BULLDOZER'
        ]);
        DB::table('type_asset')->insert([
            'type_name' => 'MOTOR GRADER'
        ]);
        DB::table('type_asset')->insert([
            'type_name' => 'COMPACTOR'
        ]);
        DB::table('type_asset')->insert([
            'type_name' => 'BACKHOE LOADER'
        ]);
        DB::table('type_asset')->insert([
            'type_name' => 'DUMP TRUCK'
        ]);
        DB::table('type_asset')->insert([
            'type_name' => 'ARTICULATED DUMP TRUCK'
        ]);
        DB::table('type_asset')->insert([
            'type_name' => 'LIGHT VEHICLE'
        ]);
        DB::table('type_asset')->insert([
            'type_name' => 'FUEL TRUCK'
        ]);
        DB::table('type_asset')->insert([
            'type_name' => 'WATER TRUCK'
        ]);
        DB::table('type_asset')->insert([
            'type_name' => 'BUS'
        ]);
        DB::table('type_asset')->insert([
            'type_name' => 'MAN HAUL'
        ]);
    }
}
