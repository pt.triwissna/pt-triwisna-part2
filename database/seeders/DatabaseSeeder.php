<?php

namespace Database\Seeders;

use App\Models\OperationsModels;
use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;

class DatabaseSeeder extends Seeder
{
    /**
     * Seed the application's database.
     */
    public function run(): void
    {
        $this->call([
             CategoryAssetSeed::class,
             TypeAssetSeed::class,
             TypeInsuranceSeed::class,
             UserSeed::class
        ]);

        DB::table('asset')->insert([
            'record_id' =>  1,
            'no_unit'   => 'BF-93',
            'type_asset_id' => 2,
            'ctgr_asset_id' => 2,
            'manufactur'    => 'TOYOTA',
            'model'         => 'Land Cruiser',
            'yom'           => 2023,
            'permit_insurance_id'  => 0
        ]);
        DB::table('asset')->insert([
            'record_id' =>  1,
            'no_unit'   => 'BC-93',
            'type_asset_id' => 2,
            'ctgr_asset_id' => 2,
            'manufactur'    => 'BMW',
            'model'         => 'M-8',
            'yom'           => 2021,
            'permit_insurance_id'  => 0
        ]);

        for ($i = 1; $i <= 100; $i++) {
            // Generate random data or use your own logic to populate the fields
            $data = [
                'operation_id' => $i,
                'record_id' => 1,
                'asset_id' => rand(1, 3), // Ganti dengan logika Anda untuk asset_id
                'date' => now()->subMonths(rand(1, 11)), // Mengurangkan bulan secara acak dari bulan sekarang
                'shift' => 'DAY',
                's_hourmeter' => rand(100, 500),
                'f_hourmeter' => rand(501, 1000),
                's_worktime' => gmdate('H:i:s', rand(1, 3600)), // Konversi detik ke format jam:menit:detik
                'f_worktime' => gmdate('H:i:s', rand(3601, 7200)),
                's_otherdelay' => gmdate('H:i:s', rand(1, 1800)),
                'f_otherdelay' => gmdate('H:i:s', rand(1801, 3600)),
                's_odometer' => rand(1000, 5000),
                'f_odometer' => rand(5001, 10000),
                'fuel' => rand(100, 500),
                'project' => 'Project ' . rand(1, 5),
                'operator' => 'Operator ' . $i,
                'trip' => rand(1, 10),
                'flg' => 'N', // Atau sesuaikan dengan logika Anda
            ];
        
            OperationsModels::create($data);
        }


        for ($i = 1; $i <= 100; $i++) {
            $startDate = now()->subMonths(rand(1, 11))->startOfDay();
            $endDate = $startDate->copy()->addDays(rand(1, 30))->endOfDay();

            DB::table('maintenance')->insert([
                'record_id' => 1,
                'asset_id' => rand(1, 3), // Ganti dengan logika Anda untuk asset_id
                's_breakdown_date' => $startDate->format('Y-m-d'),
                's_breakdown_time' => $startDate->format('H:i:s'),
                'f_breakdown_date' => $endDate->format('Y-m-d'),
                'f_breakdown_time' => $endDate->format('H:i:s'),
                'issue' => 'Issue ' . $i,
                'perform_by' => 'PerformBy ' . $i,
                'finance' => rand(100000, 1000000000),
                'flg' => 'N', // Atau sesuaikan dengan logika Anda
            ]);
        
        }

    }
}
