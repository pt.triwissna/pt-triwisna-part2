@extends('app')
@section('title-app')
    Home
@endsection
@section('navbar-title-back')
    Home
@endsection
@section('content')
    <!-- Content -->
    <div class="container-xxl flex-grow-1 container-p-y">
        <div class="row">
            <div class="col-lg-6 mb-3 order-0">
                <div class="card">
                    <div class="d-flex align-items-end row">
                        <div class="col-sm-7">
                            <div class="card-body">
                                <h4 class="card-title text-primary">Welcome {{ Auth::user()->name }}</h4>
                                <span>{{ \Carbon\Carbon::now()->locale('en')->translatedFormat('l, d F Y') }}</span>
                            </div>
                        </div>

                    </div>
                </div>
            </div>
            <div class="col-lg-6 order-1">
                <div class="row">
                    <div class="col-lg-4 mb-4">
                        <div class="card">
                            <div class="card-body text-center">
                                <div class="scrolling-container">
                                    <span class="fw-semibold d-block mb-1" id="scrolling-text">Total Data Maintenance</span>
                                    <h3 class="card-title mb-1 text-warning">{{ number_format($total_operation, 0, ',', '.') }}</h3>
                                </div>
                            </div>
                        </div>
                    </div>


                    <div class="col-lg-4 mb-4">
                        <div class="card">
                            <div class="card-body text-center">
                                <div class="scrolling-container">
                                    <span class="fw-semibold d-block mb-1" id="scrolling-text">Total Data Operation</span>
                                    <h3 class="card-title mb-1 text-warning">{{ number_format($total_maintenance, 0, ',', '.') }}</h3>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="col-lg-4 mb-4">
                        <div class="card">
                            <div class="card-body text-center">
                                <div class="scrolling-container">
                                    <span class="fw-semibold d-block mb-1" id="scrolling-text">Total Data Maintenance</span>
                                    <h3 class="card-title mb-1 text-warning">{{ number_format($total_asset, 0, ',', '.') }}</h3>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>

            <div class="col-12 col-lg-12 order-2 order-md-3 order-lg-2 mb-4">
                <div class="card">
                    <div class="row row-bordered g-0">
                        <div class="col-md-12 p-3">
                            <h5 class="card-header">Grafik Operation</h5>
                            <div>
                                <div class="card-body">
                                    <div class="" id="operation"></div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <!--/ Total Revenue -->
            <div class="col-12 col-lg-12 order-2 order-md-3 order-lg-2 mb-4">
                <div class="card">
                    <div class="row row-bordered g-0">
                        <div class="col-md-12 p-3">
                            <h5 class="card-header">Grafik Maintenance</h5>
                            <div>
                                <div class="card-body">
                                    <div class="" id="maintenance"></div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <script src="{{ asset('js/total_peminjaman-perbaikan.js') }}"></script>
        <script src="https://code.highcharts.com/highcharts.js"></script>
        <script type="text/javascript">
            var data_operation = <?php echo json_encode($total_data_operation ?? []); ?>;
            var bulan_operation = <?php echo json_encode($bulan_operation ?? []); ?>;

            Highcharts.chart('operation', {
                title: {
                    text: 'Grafik Operation'
                },
                xAxis: {
                    categories: bulan_operation
                },
                yAxis: {
                    title: {
                        text: 'Total Operation'
                    }
                },
                plotOptions: {
                    series: {
                        allowPointSelect: true
                    }
                },
                series: [{
                    name: 'Total Operation',
                    data: data_operation
                }]
            });

            var data_maintenance = <?php echo json_encode($total_finance ?? []); ?>;
            var bulan_maintenance = <?php echo json_encode($bulan_maintenance ?? []); ?>;

            Highcharts.chart('maintenance', {
                title: {
                    text: 'Maintenance Expense Grafik'
                },
                xAxis: {
                    categories: bulan_maintenance
                },
                yAxis: {
                    title: {
                        text: 'Total Expense'
                    }
                },
                plotOptions: {
                    series: {
                        allowPointSelect: true
                    }
                },
                series: [{
                    name: 'Total Expense',
                    data: data_maintenance
                }]
            });
        </script>
        
    @endsection
