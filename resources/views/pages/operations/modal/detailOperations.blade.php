@php
    // Import class Carbon
    use Carbon\Carbon;

    // Set locale ke bahasa Indonesia
    Carbon::setLocale('id');

    // Mendapatkan nama hari dalam bahasa Indonesia
    $dayInIndonesian = Carbon::parse($op->date)->translatedFormat('l');
    $monthsInIndonesian = [
        1 => 'Januari',
        2 => 'Februari',
        3 => 'Maret',
        4 => 'April',
        5 => 'Mei',
        6 => 'Juni',
        7 => 'Juli',
        8 => 'Agustus',
        9 => 'September',
        10 => 'Oktober',
        11 => 'November',
        12 => 'Desember',
    ];

    // Ubah format tampilan tanggal
    $date = Carbon::createFromFormat('Y-m-d', $op->date); // Ganti dengan tanggal yang sesuai
    $formattedDate = $date->format('d') . ' ' . $monthsInIndonesian[$date->format('n')] . ' ' . $date->format('Y');
@endphp


<div class="modal fade" id="detailModalOperation{{ $op->operation_id }}" tabindex="-1" aria-hidden="true">
  <div class="modal-dialog modal-xl" role="document">
      <div class="modal-content">
          <div class="modal-header">
              <h5 class="modal-title" id="exampleModalLabel3">History Details</h5>
              <button type="button" class="btn-close" data-bs-dismiss="modal" aria-label="Close"></button>
          </div>

          <hr>
          <div class="modal-body">
              <div class="row">
                  <div class="col-md-6">
                      <div class="row mb-3">
                          <label class="col-sm-3 col-form-label" for="basic-default-name">Day / Date</label>
                          <div class="col-sm-9">
                              <input disabled type="text" class="form-control" id="basic-default-name" value="{{ $dayInIndonesian }}, {{ $formattedDate ?? '' }}" />
                          </div>
                      </div>
                      <div class="row mb-3">
                          <label class="col-sm-3 col-form-label" for="basic-default-company">Shift</label>
                          <div class="col-sm-9">
                              <input disabled type="text" class="form-control" id="basic-default-company" value="{{ $op->shift ?? '' }}" />
                          </div>
                      </div>
                      <div class="row mb-3">
                          <label class="col-sm-3 col-form-label" for="basic-default-company">Clock</label>
                          <div class="col-sm-9">
                              <input disabled type="text" class="form-control" id="basic-default-company" value="start: {{ $op->s_worktime ?? '' }} - finish: {{ $op->f_worktime ?? '' }}" />
                          </div>
                      </div>
                      <div class="row mb-3">
                          <label class="col-sm-3 col-form-label" for="basic-default-company">Project</label>
                          <div class="col-sm-9">
                              <input disabled type="text" class="form-control" id="basic-default-company" value="{{ $op->project ?? '' }}" />
                          </div>
                      </div>
                      <div class="row mb-3">
                          <label class="col-sm-3 col-form-label" for="basic-default-company">Operator</label>
                          <div class="col-sm-9">
                              <input disabled type="text" class="form-control" id="basic-default-company" value="{{ $op->operator ?? '' }}" />
                          </div>
                      </div>
                  </div>
                  <div class="col-md-6">
                      <div class="row mb-3">
                          <label class="col-sm-3 col-form-label" for="basic-default-name">Unit Number</label>
                          <div class="col-sm-9">
                              <input disabled type="text" class="form-control" id="basic-default-name" value="{{ $op->no_unit ?? '' }}" />
                          </div>
                      </div>
                      <div class="row mb-3">
                          <label class="col-sm-3 col-form-label" for="basic-default-company">Type</label>
                          <div class="col-sm-9">
                              <input disabled type="text" class="form-control" id="basic-default-company" value="{{ $op->type_name ?? '' }}" />
                          </div>
                      </div>
                      <div class="row mb-3">
                          <label class="col-sm-3 col-form-label" for="basic-default-company">Brand</label>
                          <div class="col-sm-9">
                              <input disabled type="text" class="form-control" id="basic-default-company" value="{{ $op->manufactur ?? '' }}" />
                          </div>
                      </div>
                      <div class="row mb-3">
                          <label class="col-sm-3 col-form-label" for="basic-default-company">Model</label>
                          <div class="col-sm-9">
                              <input disabled type="text" class="form-control" id="basic-default-company" value="{{ $op->model ?? '' }}" />
                          </div>
                      </div>
                      <div class="row mb-3">
                          <label class="col-sm-3 col-form-label" for="basic-default-company">Year</label>
                          <div class="col-sm-9">
                              <input disabled type="text" class="form-control" id="basic-default-company" value="{{ $op->yom ?? '' }}" />
                          </div>
                      </div>
                  </div>
              </div>
          </div>
      </div>
  </div>
</div>
