
<div class="modal fade" id="operation{{ $op->operation_id }}" tabindex="-1" aria-hidden="true">
    <div class="modal-dialog modal-lg" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title" id="exampleModalLabel3">Detail Operation</h5>
                <button type="button" class="btn-close" data-bs-dismiss="modal" aria-label="Close"></button>
            </div>
            <hr>
            <div class="modal-body">
                <form>
                    <div class="row mb-3 ms-2">
                        <label class="col-sm-3 col-form-label" for="basic-default-name">Date</label>
                        <div class="col-sm-8">
                            <input type="text" class="form-control" id="basic-default-name" disabled value="{{ $op->date }}"/>
                        </div>
                    </div>
                    <div class="row mb-3 ms-2">
                        <label class="col-sm-3 col-form-label" for="basic-default-name">Effective Working Time</label>
                        <div class="col-sm-8">
                            <input type="text" class="form-control" id="basic-default-name" disabled value="{{ $op->s_worktime }} - {{ $op->f_worktime }}"/>
                        </div>
                    </div>
                    <div class="row mb-3 ms-2">
                        <label class="col-sm-3 col-form-label" for="basic-default-name">Hour Meter</label>
                        <div class="col-sm-8">
                            <input type="text" class="form-control" id="basic-default-name" disabled value="{{ $op->s_hourmeter }} - {{ $op->f_hourmeter }}"/>
                        </div>
                    </div>
                    <div class="row mb-3 ms-2">
                        <label class="col-sm-3 col-form-label" for="basic-default-name">Other Delay</label>
                        <div class="col-sm-8">
                            <input type="text" class="form-control" id="basic-default-name" disabled value="{{ $op->s_otherdelay }} - {{ $op->f_otherdelay }}"/>
                        </div>
                    </div>
                    <div class="row mb-3 ms-2">
                        <label class="col-sm-3 col-form-label" for="basic-default-name">Odometer</label>
                        <div class="col-sm-8">
                            <input type="text" class="form-control" id="basic-default-name" disabled value="{{ $op->s_odometer }} - {{ $op->f_odometer }}"/>
                        </div>
                    </div>
                    <div class="row mb-3 ms-2">
                        <label class="col-sm-3 col-form-label" for="basic-default-name">Fuel Inflow</label>
                        <div class="col-sm-8">
                            <input type="text" class="form-control" id="basic-default-name" disabled value="{{ $op->fuel }}"/>
                        </div>
                    </div>
                    <div class="row mb-3 ms-2">
                        <label class="col-sm-3 col-form-label" for="basic-default-name">Trip</label>
                        <div class="col-sm-8">
                            <input type="text" class="form-control" id="basic-default-name" disabled value="{{ $op->trip }}"/>
                        </div>
                    </div>
                    <div class="row mb-3 ms-2">
                        <label class="col-sm-3 col-form-label" for="basic-default-name">Operator</label>
                        <div class="col-sm-8">
                            <input type="text" class="form-control" id="basic-default-name" disabled value="{{ $op->operator }}"/>
                        </div>
                    </div>
                    <div class="row mb-3 ms-2">
                        <label class="col-sm-3 col-form-label" for="basic-default-name">Unit Number</label>
                        <div class="col-sm-8">
                            <input type="text" class="form-control" id="basic-default-name" disabled value="{{ $op->no_unit }}"/>
                        </div>
                    </div>
                    <div class="row mb-3 ms-2">
                        <label class="col-sm-3 col-form-label" for="basic-default-name">Type Asset</label>
                        <div class="col-sm-8">
                            <input type="text" class="form-control" id="basic-default-name" disabled value="{{ $op->type_name }}"/>
                        </div>
                    </div>
                    <div class="row mb-3 ms-2">
                        <label class="col-sm-3 col-form-label" for="basic-default-name">Category Asset</label>
                        <div class="col-sm-8">
                            <input type="text" class="form-control" id="basic-default-name" disabled value="{{ $op->ctgr_name }}"/>
                        </div>
                    </div>
                    <div class="row mb-3 ms-2">
                        <label class="col-sm-3 col-form-label" for="basic-default-name">Manufacture</label>
                        <div class="col-sm-8">
                            <input type="text" class="form-control" id="basic-default-name" disabled value="{{ $op->manufactur }}"/>
                        </div>
                    </div>
                    <div class="row mb-3 ms-2">
                        <label class="col-sm-3 col-form-label" for="basic-default-name">Model</label>
                        <div class="col-sm-8">
                            <input type="text" class="form-control" id="basic-default-name" disabled value="{{ $op->model }}"/>
                        </div>
                    </div>
                    <div class="row mb-3 ms-2">
                        <label class="col-sm-3 col-form-label" for="basic-default-name">Created By</label>
                        <div class="col-sm-8">
                            <input type="text" class="form-control" id="basic-default-name" disabled value="{{$op->username}}"/>
                        </div>
                    </div>
                </form>
            </div>
        </div>
    </div>
</div>
