<div class="modal fade" id="m_operation{{ $i->operation_id }}" tabindex="-1" aria-hidden="true">
    <div class="modal-dialog modal-lg" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title" id="exampleModalLabel3">Detailed operating data revised</h5>
                <button type="button" class="btn-close" data-bs-dismiss="modal" aria-label="Close"></button>
            </div>
            <hr>
            <div class="modal-body">
                <form>
                    <div class="row mb-3 ms-2">
                        <label class="col-sm-3 col-form-label" for="basic-default-name">Date</label>
                        <div class="col-sm-8">
                            <input type="text" class="form-control" id="basic-default-name" disabled
                                value="{{ $i->date }}" />
                        </div>
                    </div>
                    <div class="row mb-3 ms-2">
                        <label class="col-sm-3 col-form-label" for="basic-default-name">Effective Working Time</label>
                        <div class="col-sm-8">
                            <input type="text" class="form-control" id="basic-default-name" disabled
                                value="{{ $i->s_worktime }} - {{ $i->f_worktime }}" />
                        </div>
                    </div>
                    <div class="row mb-3 ms-2">
                        <label class="col-sm-3 col-form-label" for="basic-default-name">Hour Meter</label>
                        <div class="col-sm-8">
                            <input type="text" class="form-control" id="basic-default-name" disabled
                                value="{{ $i->s_hourmeter }} - {{ $i->f_hourmeter }}" />
                        </div>
                    </div>
                    <div class="row mb-3 ms-2">
                        <label class="col-sm-3 col-form-label" for="basic-default-name">Other Delay</label>
                        <div class="col-sm-8">
                            <input type="text" class="form-control" id="basic-default-name" disabled
                                value="{{ $i->s_otherdelay }} - {{ $i->f_otherdelay }}" />
                        </div>
                    </div>
                    <div class="row mb-3 ms-2">
                        <label class="col-sm-3 col-form-label" for="basic-default-name">Odometer</label>
                        <div class="col-sm-8">
                            <input type="text" class="form-control" id="basic-default-name" disabled
                                value="{{ $i->s_odometer }} - {{ $i->f_odometer }}" />
                        </div>
                    </div>
                    <div class="row mb-3 ms-2">
                        <label class="col-sm-3 col-form-label" for="basic-default-name">Fuel Inflow</label>
                        <div class="col-sm-8">
                            <input type="text" class="form-control" id="basic-default-name" disabled
                                value="{{ $i->fuel }}" />
                        </div>
                    </div>
                    <div class="row mb-3 ms-2">
                        <label class="col-sm-3 col-form-label" for="basic-default-name">Trip</label>
                        <div class="col-sm-8">
                            <input type="text" class="form-control" id="basic-default-name" disabled
                                value="{{ $i->trip }}" />
                        </div>
                    </div>
                    <div class="row mb-3 ms-2">
                        <label class="col-sm-3 col-form-label" for="basic-default-name">Operator</label>
                        <div class="col-sm-8">
                            <input type="text" class="form-control" id="basic-default-name" disabled
                                value="{{ $i->operator }}" />
                        </div>
                    </div>
                    <div class="row mb-3 ms-2">
                        <label class="col-sm-3 col-form-label" for="basic-default-name">Unit Number</label>
                        <div class="col-sm-8">
                            <input type="text" class="form-control" id="basic-default-name" disabled
                                value="{{ $i->no_unit }}" />
                        </div>
                    </div>

                    <div class="row mb-3 ms-2">
                        <label class="col-sm-3 col-form-label" for="basic-default-name">Manufacture</label>
                        <div class="col-sm-8">
                            <input type="text" class="form-control" id="basic-default-name" disabled
                                value="{{ $i->manufactur }}" />
                        </div>
                    </div>
                    <div class="row mb-3 ms-2">
                        <label class="col-sm-3 col-form-label" for="basic-default-name">Model</label>
                        <div class="col-sm-8">
                            <input type="text" class="form-control" id="basic-default-name" disabled
                                value="{{ $i->model }}" />
                        </div>
                    </div>

                </form>
            </div>
        </div>
    </div>
</div>