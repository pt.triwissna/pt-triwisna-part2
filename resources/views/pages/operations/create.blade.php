@extends('app')
@section('title-app')
    Create Operations
@endsection
@section('link-back')
    {{ route('index.operations') }}
@endsection


@section('content')
    <div class="card p-3">
        <div class="container-view">
            <a href="{{ route('index.operations') }}">
                <button type="button" class="btn btn-primary">Back</button>
            </a>
            <hr>
            <br>
            <form action="{{ route('store.operations') }}" method="post">
                @csrf
                <div class="row justify-content-start mb-3">
                    <label class="col-sm-2 col-form-label text-center" for="basic-default-name">Tanggal</label>
                    <div class="col-sm-6">
                        <input type="date" class="form-control @error('date') is-invalid @enderror"
                            id="basic-default-name" name="date" value="{{ date('Y-m-d') }}" />
                        @error('date')
                            <div class="invalid-feedback">
                                {{ $message }}
                            </div>
                        @enderror
                    </div>
                </div>
                <div class="row justify-content-start mb-3">
                    <label class="col-sm-2 col-form-label text-center" for="basic-default-name">Shift</label>
                    <div class="col-sm-6">
                        <select name="shift" class="form-select @error('shift') is-invalid @enderror" id="exampleFormControlSelect1"
                            aria-label="Default select example" >
                            <option></option>
                            <option value="DAY" {{ old('shift') == 'DAY' ? 'selected' : '' }}>DAY</option>
                            <option value="NIGHT" {{ old('shift') == 'NIGHT' ? 'selected' : '' }}>NIGHT</option>
                        </select>
                        @error('shift')
                            <div class="invalid-feedback">
                                {{ $message }}
                            </div>
                        @enderror
                    </div>
                </div>
                <div class="row justify-content-start mb-3">
                    <label class="col-sm-2 col-form-label text-center" for="basic-default-name">Project</label>
                    <div class="col-sm-6">
                        <input type="text" name="project" class="form-control @error('project') is-invalid @enderror"
                            id="basic-default-name" value="{{ old('project') }}" />
                        @error('project')
                            <div class="invalid-feedback">
                                {{ $message }}
                            </div>
                        @enderror
                    </div>
                </div>
                <div class="row justify-content-start mb-3">
                    <label class="col-sm-2 col-form-label text-center" for="basic-default-name">Operator</label>
                    <div class="col-sm-6">
                        <input type="text" name="operator" class="form-control @error('operator') is-invalid @enderror"
                            id="basic-default-name" value="{{ old('operator') }}" />
                        @error('operator')
                            <div class="invalid-feedback">
                                {{ $message }}
                            </div>
                        @enderror
                    </div>
                </div>
                <div class="row justify-content-start mb-3">
                    <label class="col-sm-2 col-form-label text-center" for="basic-default-name">Unit</label>
                    <div class="col-sm-6">
                        <select name="asset_id" class="form-select  @error('asset_id') is-invalid @enderror" id="exampleFormControlSelect1"
                            aria-label="Default select example">
                            <option></option>
                            @foreach ($unit as $a)
                                <option value="{{ $a->asset_id }}" {{ old('asset_id') == $a->asset_id ? 'selected' : '' }}>{{ $a->no_unit }} - {{ $a->manufactur }}</option>
                            @endforeach
                        </select>
                        @error('asset_id')
                            <div class="invalid-feedback">
                                {{ $message }}
                            </div>
                        @enderror
                    </div>
                </div>
                <div class="row justify-content-start mb-3">
                    <label class="col-sm-2 col-form-label text-center" for="basic-default-name">Fuel Inflow<small>(Liter)</small></label>
                    <div class="col-sm-6">
                        <input type="number" name="fuel" class="form-control @error('fuel') is-invalid @enderror"
                            id="basic-default-name" value="{{ old('fuel') }}" />
                        @error('fuel')
                            <div class="invalid-feedback">
                                {{ $message }}
                            </div>
                        @enderror
                    </div>
                </div>
                <div class="row justify-content-start mb-3">
                    <label class="col-sm-2 col-form-label text-center" for="basic-default-name">Trip</label>
                    <div class="col-sm-6">
                        <input type="number" name="trip" class="form-control @error('trip') is-invalid @enderror"
                            id="basic-default-name" value="{{ old('trip') }}" />
                        @error('trip')
                            <div class="invalid-feedback">
                                {{ $message }}
                            </div>
                        @enderror
                    </div>
                </div>
                <div class="row justify-content-start mb-3">
                    <label class="col-sm-2 col-form-label text-center" for="basic-default-name">Worktime</label>
                    <div class="col-sm-6">
                        <div class="input-group">
                            <input type="time" class="form-control @error('s_worktime') is-invalid @enderror" name="s_worktime" value="{{ old('s_worktime') }}" />
                            <input type="time" class="form-control @error('f_worktime') is-invalid @enderror" name="f_worktime" value="{{ old('f_worktime') }}" />
                        </div>
                        @error('s_worktime', 'f_worktime')
                            <div class="invalid-feedback">
                                {{ $message }}
                            </div>
                        @enderror
                    </div>
                </div>
                <div class="row justify-content-start mb-3">
                    <label class="col-sm-2 col-form-label text-center" for="basic-default-name">OdoMeter</label>
                    <div class="col-sm-6">
                        <div class="input-group">
                            <input type="number" class="form-control @error('s_odometer') is-invalid @enderror" placeholder="Start" aria-label="Start" name="s_odometer" value="{{ old('s_odometer') }}" />
                            <input type="number" class="form-control @error('f_odometer') is-invalid @enderror" placeholder="Finish" aria-label="Finish" name="f_odometer" value="{{ old('f_odometer') }}" />
                        </div>
                        @error('s_odometer', 'f_odometer')
                            <div class="invalid-feedback">
                                {{ $message }}
                            </div>
                        @enderror
                    </div>
                </div>
                <div class="row mb-3 justify-content-start">
                    <label class="col-sm-2 col-form-label text-center" for="basic-default-name">Hour Meter</label>
                    <div class="col-sm-6">
                        <div class="input-group">
                            <input type="number" class="form-control @error('s_hourmeter') is-invalid @enderror" placeholder="Start" aria-label="Start" name="s_hourmeter" value="{{ old('s_hourmeter') }}" />
                            <input type="number" class="form-control @error('f_hourmeter') is-invalid @enderror" placeholder="Finish" aria-label="Finish" name="f_hourmeter" value="{{ old('f_hourmeter') }}" />
                        </div>
                        @error('s_hourmeter.numeric')
            <div class="invalid-feedback">
                {{ $message }}
            </div>
        @enderror
        @error('f_hourmeter.numeric')
            <div class="invalid-feedback">
                {{ $message }}
            </div>
        @enderror
                    </div>
                </div>
                <div class="row mb-3 justify-content-start">
                    <label class="col-sm-2 col-form-label text-center" for="basic-default-name">Other Delay</label>
                    <div class="col-sm-6">
                        <div class="input-group mb-3">
                            <input type="time" class="form-control @error('s_otherdelay') is-invalid @enderror" name="s_otherdelay" value="{{ old('s_otherdelay') }}" />
                            <input type="time" class="form-control @error('f_otherdelay') is-invalid @enderror" name="f_otherdelay" value="{{ old('f_otherdelay') }}" />
                        </div>
                        @error('s_otherdelay', 'f_otherdelay')
                            <div class="invalid-feedback">
                                {{ $message }}
                            </div>
                        @enderror
                    </div>
                </div>
                <div class="row justify-content-end">
                    <div class="col-sm-10">
                        <button type="submit" class="btn btn-primary">Submit</button>
                    </div>
                </div>
            </form>
        </div>
    </div>
    <script>
        document.addEventListener("DOMContentLoaded", function() {
            var dateInput = document.getElementById("date");
            var pickDateButton = document.getElementById("pickDateButton");

            pickDateButton.addEventListener("click", function() {
                dateInput.click();
            });

            dateInput.addEventListener("change", function() {
                if (this.value) {
                    var formattedDate = formatDateToDMY(this.value);
                    this.value = formattedDate;
                }
            });
        });
    </script>
@endsection
