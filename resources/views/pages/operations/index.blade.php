@extends('app')
@section('title-app')
    Operations
@endsection
@section('navbar-title-back')
    Operation
@endsection
@section('content')

    @if (session('success'))
        @include('partials.alert-success')
    @endif

    @if (session('error'))
        @include('partials.alert-error')
    @endif
    <div class="card p-3">
        <div class="d-grid gap-2 d-md-flex justify-content-md-end">
            @if ($loggedInUser->role->role == 'admin' || $loggedInUser->role->role == 'manager')
                <a href="{{ route('create.operations') }}" class="btn btn-primary me-md-2 pe-5 ps-5">Add Operation</a>
            @endif
            @if ($loggedInUser->role->role == 'supervisor')
                {{-- <button class="btn btn-primary" type="button" data-bs-toggle="offcanvas" data-bs-target="#revisionrequest"
                    aria-controls="offcanvasBackdrop"><i class='bx bx-history'></i>
                    Revision Request <span class="badge rounded-pill badge-center h-px-20 w-px-20 bg-danger">3</span>
                </button> --}}
                <button class="btn btn-primary" type="button" data-bs-toggle="offcanvas" data-bs-target="#revisionhistory"
                    aria-controls="offcanvasBackdrop"><i class='bx bx-history'></i>
                    Revision
                </button>
                @include('pages.operations.modal.historyMessage')
            @endif
        </div>
        <form action="{{ route('index.operations') }}" method="get">
            @csrf

            <div class="row mb-5">

                <div class="col-md-2">
                    <label for="" class="fw-bold">No Asset</label>
                    <input name="no_unit" type="text" class="form-control" placeholder="No. Asset"
                        value="{{ isset($_GET['no_unit']) ? $_GET['no_unit'] : '' }}">
                </div>
                <div class="col-md-2">
                    <label for="" class="fw-bold">Date</label>
                    <input name="date" type="date" class="form-control"
                        value="{{ isset($_GET['date']) ? $_GET['date'] : '' }}">
                </div>
                <div class="col-md-2">
                    <label for="" class="fw-bold">Shift</label>
                    <select name="shift" class="form-select">
                        <option value="">- All -</option>
                        <option value="DAY" {{ isset($_GET['shift']) && $_GET['shift'] == 'DAY' ? 'selected' : '' }}>DAY
                        </option>
                        <option value="NIGHT" {{ isset($_GET['shift']) && $_GET['shift'] == 'NIGHT' ? 'selected' : '' }}>
                            NIGHT
                        </option>
                    </select>
                </div>

                <div class="col-md-3">
                    <button type="submit" class="btn btn-primary mt-4">Search</button>
                </div>
            </div>
        </form>

        <div class="table-responsive text-nowrap">
            <table class="table table-striped table-hover">
                <thead>
                    <tr class="table-active">
                        <th class="fw-bold">Action</th>
                        <th class="fw-bold">Date</th>
                        <th class="fw-bold">Shift</th>
                        <th class="fw-bold">Asset</th>
                        <th class="fw-bold">Working Time</th>
                        <th class="fw-bold">Hour Meter</th>
                        <th class="fw-bold">Other Delay</th>
                        <th class="fw-bold">Odometer</th>
                        <th class="fw-bold">Project</th>
                    </tr>
                </thead>
                <tbody class="table-border-bottom-0">
                    @if (count($operations) < 1)
                        <tr>
                            <td colspan="10" style="padding: 20px; font-size: 20px;"><span>No data found</span>
                            </td>
                        </tr>
                    @else
                        @foreach ($operations as $op)
                            @include('pages.operations.modal.detail_model')
                            @include('pages.operations.modal.messageOperation')
                            <tr>

                                <td>
                                    <button class="btn btn-primary btn-icon btm-sm" data-bs-toggle="modal"
                                        data-bs-target="#operation{{ $op->operation_id }}"><i
                                            class='bx bx-detail'></i></button>
                                    @if ($loggedInUser->role->role == 'supervisor')
                                        <a class="btn btn-icon btn-warning btm-sm"
                                            href="{{ route('pageEditOperation', ['id' => $op->operation_id]) }}"><i
                                                class='bx bxs-edit'></i></a>
                                        @if ($op->flg == 'N')
                                            <button class="btn btn-icon btn-success btm-sm" data-bs-toggle="modal"
                                                data-bs-target="#message{{ $op->operation_id }}"><i
                                                    class='bx bx-message-alt-edit'></i></button>
                                        @endif
                                    @endif
                                    @if ($loggedInUser->role->role == 'manager')
                                        <a class="btn btn-icon btn-warning btm-sm"
                                            href="{{ route('pageEditOperation', ['id' => $op->operation_id]) }}"><i
                                                class='bx bxs-edit'></i></a>
                                        <button class="btn btn-danger btn-icon btm-sm" data-bs-toggle="modal"
                                            data-bs-target="#delete{{ $op->operation_id }}"><i
                                                class='bx bxs-trash'></i></button>
                                                @include('pages.operations.modal.confirm')
                                    @endif
                                </td>
                                <td>{{ $op->date }}</td>
                                <td>{{ $op->shift }}</td>
                                <td>{{ $op->no_unit }}, {{ $op->manufactur }}</td>
                                <td>{{ $op->s_worktime }} - {{ $op->f_worktime }}</td>
                                <td>{{ $op->s_hourmeter }} - {{ $op->f_hourmeter }}</td>
                                <td>{{ $op->s_otherdelay }} - {{ $op->f_otherdelay }}</td>
                                <td>{{ $op->s_odometer }} - {{ $op->f_odometer }}</td>
                                <td>{{ $op->project }}</td>
                            </tr>
                        @endforeach
                    @endif

                </tbody>
            </table>
        </div>
        <div class="row pt-5">
            <div class="col-lg-10">
                <ul class="pagination">
                    {{ $operations->links() }}
                </ul>
                <br>
                <span>Total data {{ $total[0]->totalData }}, pages {{ $operations->currentPage() }} of
                    {{ $operations->lastPage() }}</span>
            </div>
        </div>
    </div>
@endsection
