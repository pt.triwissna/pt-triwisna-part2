@extends('app')
@section('title-app')
    Data Asset
@endsection
@section('navbar-title-back')
    Data Asset
@endsection
@section('content')
    @if (session('success'))
        @include('partials.alert-success')
    @endif

    @if (session('error'))
        @include('partials.alert-error')
    @endif
    <div class="card p-3">

        <div class="d-grid gap-2 d-md-flex justify-content-md-end">
            @if ($loggedInUser->role->role == 'admin' || $loggedInUser->role->role == 'manager')
                <a href="{{ route('create.asset') }}" class="btn btn-primary me-md-2 pe-5 ps-5">Add Asset</a>
            @endif
        </div>

        <form action="{{ route('index.asset') }}" method="get">
            @csrf
            @if ($loggedInUser->role->role != 'admin')
                <div class="row mb-5 mt-5">
                @else
                    <div class="row mb-5">
            @endif
            <div class="col-md-2">
                <label for="" class="fw-bold">No. Unit</label>
                <input name="no_unit" type="text" class="form-control"
                    value="{{ isset($_GET['no_unit']) ? $_GET['no_unit'] : '' }}" placeholder="No. Unit">
            </div>
            <div class="col-md-2">
                <label for="" class="fw-bold">Asset Type</label>
                <select name="type_asset" class="form-select">
                    <option value="">- All -</option>
                    @foreach ($type_asset as $o)
                        <option value="{{ $o->type_asset_id }}"
                            {{ isset($_GET['type_asset']) && (int) $_GET['type_asset'] === $o->type_asset_id ? 'selected' : '' }}>
                            {{ $o->type_name }}</option>
                    @endforeach
                </select>
            </div>
            <div class="col-md-2">
                <label for="" class="fw-bold">Asset Category</label>
                <select name="ctgr_asset" class="form-select">
                    <option value="">- All -</option>
                    @foreach ($ctgr_asset as $o)
                        <option value="{{ $o->ctgr_asset_id }}"
                            {{ isset($_GET['ctgr_asset']) && (int) $_GET['ctgr_asset'] === $o->ctgr_asset_id ? 'selected' : '' }}>
                            {{ $o->ctgr_name }}</option>
                    @endforeach
                </select>
            </div>
            <div class="col-md-2">
                <button type="submit" class="btn btn-primary mt-4">Search</button>
            </div>
    </div>
    </form>

    <div class="table-responsive text-nowrap">
        <table class="table table-striped table-hover">
            <thead>
                <tr class="table-active">
                    <th class="fw-bold">Action</th>
                    <th class="fw-bold">No. Unit</th>
                    <th class="fw-bold">Type</th>
                    <th class="fw-bold">Category</th>
                    <th class="fw-bold">Manufacture</th>
                    <th class="fw-bold">Model</th>
                    <th class="fw-bold">YOM</th>
                    <th class="fw-bold">Insurance</th>
                </tr>
            </thead>
            <tbody class="table-border-bottom-0">
                @if (count($asset) < 1)
                    <tr>
                        <td colspan="8" style="padding: 20px; font-size: 20px;"><span>No data found</span>
                        </td>
                    </tr>
                @else
                    @foreach ($asset as $i)
                        @include('pages.asset.detail')
                        
                        <tr>
                            <td>
                                <button class="btn btn-primary btn-icon btm-sm" data-bs-toggle="modal"
                                    data-bs-target="#detaildataunit{{ $i->asset_id }}"><i
                                        class='bx bx-detail'></i></button>
                                @if ($loggedInUser->role->role == 'supervisor' || $loggedInUser->role->role == 'manager')
                                    <a class="btn btn-icon btn-warning btm-sm"
                                        href="{{ route('editPage', ['id' => $i->asset_id]) }}"><i
                                            class='bx bxs-edit'></i></a>
                                @endif
                                @if ($loggedInUser->role->role == 'manager')
                                    <button class="btn btn-danger btn-icon btm-sm" data-bs-toggle="modal"
                                        data-bs-target="#delete{{ $i->asset_id }}"><i
                                            class='bx bxs-trash'></i></button>
                                            @include('pages.asset.confirm')
                                @endif

                            </td>
                            <td>{{ $i->no_unit }}</td>
                            <td>{{ $i->type_name }}</td>
                            <td>{{ $i->ctgr_name }}</td>
                            <td>{{ $i->manufactur }}</td>
                            <td>{{ $i->model }}</td>
                            <td>{{ $i->yom }}</td>
                            <td>{{ $i->insurance_issued }} - {{ $i->insurance_expired }}</td>
                        </tr>
                    @endforeach
                @endif
            </tbody>
        </table>
    </div>
    <div class="row pt-5">
        <div class="col-lg-10">
            <ul class="pagination">
                {{ $asset->links() }}
            </ul>
            <br>
            <span>Total data {{ $total[0]->totalData }}, pages {{ $asset->currentPage() }} of
                {{ $asset->lastPage() }}</span>
        </div>
    </div>
    </div>
@endsection
