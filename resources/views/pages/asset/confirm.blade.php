<div class="modal modal-top fade" id="delete{{ $i->asset_id }}" tabindex="-1">
    <div class="modal-dialog">
        <form class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title" id="modalTopTitle">Confirm Deletion</h5>
                <button type="button" class="btn-close" data-bs-dismiss="modal" aria-label="Close"></button>
            </div>
            <div class="modal-body">
                Are you sure you want to delete this data?
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-outline-secondary" data-bs-dismiss="modal">
                    Close
                </button>
                <a href="{{ route('delete-asset', ['id' => $i->asset_id]) }}" class="btn btn-danger">Delete</a>
            </div>
        </form>
    </div>
</div>
