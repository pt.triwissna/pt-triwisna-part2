@extends('app')
@section('title-app')
    Add Data Asset
@endsection
@section('navbar-title-back')
    Data Asset
@endsection
@section('navbar-title-target')
    / Edit
@endsection
@section('link-back')
    {{ route('index.asset') }}
@endsection
@section('content')
    @if (session('error'))
        @include('partials.alert-error')
    @endif
    <div class="card p-3">
        <div class="container-view">
            <a href="{{ route('index.asset') }}">
                <button type="button" class="btn btn-primary">Back</button>
            </a>
            <hr>
            <br>
            <form action="{{ route('editAsset', ['id' => $asset->asset_id]) }}" method="post">
                @csrf
                <div class="row justify-content-start mb-3">
                    <label class="col-sm-2 col-form-label text-center" for="basic-default-name">Type</label>
                    <div class="col-sm-6">
                        <select name="type_asset_id" class="form-select @error('id_type') is-invalid @enderror"
                            id="exampleFormControlSelect1" aria-label="Default select example">
                            @foreach ($type as $i)
                                <option value="{{ $i->type_asset_id }}" {{ (old('type_asset_id') ?? $asset->type_asset_id) == $i->type_asset_id  ? 'selected' : '' }}>{{ $i->type_name }}</option>
                            @endforeach
                        </select>
                        @error('type_asset_id')
                            <div class="invalid-feedback">
                                {{ $message }}
                            </div>
                        @enderror
                    </div>
                </div>
                <div class="row justify-content-start mb-3">
                    <label class="col-sm-2 col-form-label text-center" for="basic-default-name">Category</label>
                    <div class="col-sm-6">
                        <select name="ctgr_asset_id" class="form-select @error('id_ctgr') is-invalid @enderror"
                            id="exampleFormControlSelect2" aria-label="Default select example">
                            @foreach ($category as $a)
                                <option value="{{ $a->ctgr_asset_id }}" {{ (old('ctgr_asset_id') ?? $asset->ctgr_asset_id) == $a->ctgr_asset_id ? 'selected' : '' }}>{{ $a->ctgr_name }}</option>
                            @endforeach
                        </select>
                        @error('ctgr_asset_id')
                            <div class="invalid-feedback">
                                {{ $message }}
                            </div>
                        @enderror
                    </div>
                </div>
                <div class="row justify-content-start mb-3">
                    <label class="col-sm-2 col-form-label text-center" for="basic-default-name">manufactur</label>
                    <div class="col-sm-6">
                        <input type="text" name="manufactur"
                            class="form-control @error('manufacture') is-invalid @enderror"
                            value="{{ old('manufacture', $asset->manufactur) }}" id="basic-default-name" />
                        @error('manufactur')
                            <div class="invalid-feedback">
                                {{ $message }}
                            </div>
                        @enderror
                    </div>
                </div>
                <div class="row justify-content-start mb-3">
                    <label class="col-sm-2 col-form-label text-center" for="basic-default-name">Model</label>
                    <div class="col-sm-6">
                        <input type="text" name="model" class="form-control @error('model') is-invalid @enderror"
                            value="{{ old('model', $asset->model) }}" id="basic-default-name" />
                        @error('model')
                            <div class="invalid-feedback">
                                {{ $message }}
                            </div>
                        @enderror
                    </div>
                </div>
                <div class="row justify-content-start mb-3">
                    <label class="col-sm-2 col-form-label text-center" for="basic-default-name">YOM</label>
                    <div class="col-sm-6">
                        <input type="number" name="yom" class="form-control @error('yom') is-invalid @enderror"
                            value="{{ old('yom', $asset->yom) }}" id="basic-default-name" placeholder="yyyy" />
                        @error('yom')
                            <div class="invalid-feedback">
                                {{ $message }}
                            </div>
                        @enderror
                    </div>
                </div>
                <div class="row justify-content-start mb-3">
                    <label class="col-sm-2 col-form-label text-center" for="basic-default-name">No. Unit</label>
                    <div class="col-sm-6">
                        <input type="text" name="no_unit" class="form-control @error('no_unit') is-invalid @enderror"
                            value="{{ old('no_unit', $asset->no_unit) }}" id="basic-default-name" />
                        @error('no_unit')
                            <div class="invalid-feedback">
                                {{ $message }}
                            </div>
                        @enderror
                    </div>
                </div>
                <div class="row  justify-content-start mb-3">
                    <label class="col-sm-2 col-form-label text-center" for="basic-default-name">Insurance Type</label>
                    <div class="col-sm-6">
                        <select name="insurance_type_id" class="form-select" id="exampleFormControlSelect3"
                            aria-label="Default select example">
                            <option></option>
                            @foreach ($insurance as $i)
                                <option value="{{ $i->insurance_type_id }}" {{ (old('insurance_type_id') ?? optional($permit)->value('insurance_type_id')) == $i->insurance_type_id ? 'selected' : '' }}>{{ $i->insurance_type_name }}</option>
                            @endforeach
                        </select>
                    </div>
                </div>
                <div class="row justify-content-start mb-3">
                    <label class="col-sm-2 col-form-label text-center" for="basic-default-name">Insurance</label>
                    <div class="col-sm-6">
                        <div class="row">
                            <div class="col-5">
                                <input type="number" name="insurance_issued" class="form-control" id="basic-default-name"
                                       maxlength="4" placeholder="yyyy (from)" value="{{ optional($permit)->value('insurance_issued') }}" />
                            </div>
                            <div class="col-1 text-center">
                                <span class="fw-bold fs-4">-</span>
                            </div>
                            <div class="col-5">
                                <input type="number" name="insurance_expired" class="form-control" id="basic-default-name"
                                       maxlength="4" placeholder="yyyy (until)" value="{{ optional($permit)->value('insurance_expired') }}" />
                            </div>
                        </div>
                    </div>
                </div>
                
                </div>
                <div class="row justify-content-start mb-3">
                    <label class="col-sm-2 col-form-label text-center" for="basic-default-name">STNK</label>
                    <div class="col-sm-6">
                        <div class="row">
                            <div class="col-5">
                                <input type="number" name="stnk_issued" class="form-control" id="basic-default-name"
                                    maxlength="4" placeholder="yyyy (from)" value="{{ optional($permit)->value('stnk_issued') }}"/>
                            </div>
                            <div class="col-1 text-center">
                                <span class="fw-bold fs-4">-</span>
                            </div>
                            <div class="col-5">
                                <input type="number" name="stnk_expired" class="form-control" id="basic-default-name"
                                    maxlength="4" placeholder="yyyy (until)" value="{{ optional($permit)->value('stnk_expired') }}"/>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="row justify-content-start mb-3">
                    <label class="col-sm-2 col-form-label text-center" for="basic-default-name">KIR</label>
                    <div class="col-sm-6">
                        <div class="row">
                            <div class="col-5">
                                <input type="number" name="kir_issued" class="form-control" id="basic-default-name"
                                    maxlength="4" placeholder="yyyy (from)" value="{{ optional($permit)->value('kir_issued') }}" />
                            </div>
                            <div class="col-1 text-center">
                                <span class="fw-bold fs-4">-</span>
                            </div>
                            <div class="col-5">
                                <input type="number" name="kir_expired" class="form-control" id="basic-default-name"
                                    maxlength="4" placeholder="yyyy (until)" value="{{ optional($permit)->value('kir_expired') }}" />
                            </div>
                        </div>
                    </div>
                </div>

                <div class="row justify-content-end mt-2 mb-5">
                    <div class="col-sm-10">
                        <button type="submit" class="btn btn-primary">Submit</button>
                    </div>
                </div>
            </form>
        </div>
    </div>

@endsection
