@extends('app')
@section('title-app')
    Add Data Asset
@endsection
@section('navbar-title-back')
    Data Asset
@endsection
@section('navbar-title-target')
    / Add
@endsection
@section('link-back')
    {{ route('index.asset') }}
@endsection
@section('content')
    @if (session('error'))
        @include('partials.alert-error')
    @endif
    <div class="card p-3">
        <div class="container-view">
            <a href="{{ route('index.asset') }}">
                <button type="button" class="btn btn-primary">Back</button>
            </a>
            <hr>
            <br>
            <form action="{{ route('store.asset') }}" method="post">
                @csrf
                <div class="row justify-content-start mb-3">
                    <label class="col-sm-2 col-form-label text-center" for="basic-default-name">Type</label>
                    <div class="col-sm-6">
                        <select name="id_type" class="form-select @error('id_type') is-invalid @enderror"
                            id="exampleFormControlSelect1" aria-label="Default select example">
                            <option></option>
                            @foreach ($type as $type)
                                <option value="{{ $type->type_asset_id }}" {{ old('id_type') == $type->type_asset_id ? 'selected' : '' }}>{{ $type->type_name }}</option>
                            @endforeach
                        </select>
                        @error('id_type')
                            <div class="invalid-feedback">
                                {{ $message }}
                            </div>
                        @enderror
                    </div>
                </div>
                <div class="row justify-content-start mb-3">
                    <label class="col-sm-2 col-form-label text-center" for="basic-default-name">Category</label>
                    <div class="col-sm-6">
                        <select name="id_ctgr" class="form-select @error('id_ctgr') is-invalid @enderror"
                            id="exampleFormControlSelect2" aria-label="Default select example">
                            <option></option>
                            @foreach ($category as $category)
                                <option value="{{ $category->ctgr_asset_id }}"  {{ old('id_ctgr') == $category->ctgr_asset_id ? 'selected' : '' }}>{{ $category->ctgr_name }}</option>
                            @endforeach
                        </select>
                        @error('id_ctgr')
                            <div class="invalid-feedback">
                                {{ $message }}
                            </div>
                        @enderror
                    </div>
                </div>
                <div class="row justify-content-start mb-3">
                    <label class="col-sm-2 col-form-label text-center" for="basic-default-name">manufactur</label>
                    <div class="col-sm-6">
                        <input type="text" name="manufacture"
                            class="form-control @error('manufacture') is-invalid @enderror"
                            value="{{ old('manufacture') }}" id="basic-default-name" />
                        @error('manufacture')
                            <div class="invalid-feedback">
                                {{ $message }}
                            </div>
                        @enderror
                    </div>
                </div>
                <div class="row justify-content-start mb-3">
                    <label class="col-sm-2 col-form-label text-center" for="basic-default-name">Model</label>
                    <div class="col-sm-6">
                        <input type="text" name="model" class="form-control @error('model') is-invalid @enderror"
                            value="{{ old('model') }}" id="basic-default-name" />
                        @error('model')
                            <div class="invalid-feedback">
                                {{ $message }}
                            </div>
                        @enderror
                    </div>
                </div>
                <div class="row justify-content-start mb-3">
                    <label class="col-sm-2 col-form-label text-center" for="basic-default-name">YOM</label>
                    <div class="col-sm-6">
                        <input type="number" name="yom" class="form-control @error('yom') is-invalid @enderror"
                            value="{{ old('yom') }}" id="basic-default-name" placeholder="yyyy" />
                        @error('yom')
                            <div class="invalid-feedback">
                                {{ $message }}
                            </div>
                        @enderror
                    </div>
                </div>
                <div class="row justify-content-start mb-3">
                    <label class="col-sm-2 col-form-label text-center" for="basic-default-name">No. Unit</label>
                    <div class="col-sm-6">
                        <input type="text" name="no_unit" class="form-control @error('no_unit') is-invalid @enderror"
                            value="{{ old('no_unit') }}" id="basic-default-name" />
                        @error('no_unit')
                            <div class="invalid-feedback">
                                {{ $message }}
                            </div>
                        @enderror
                    </div>
                </div>
                <div class="row  justify-content-start mb-3">
                    <label class="col-sm-2 col-form-label text-center" for="basic-default-name">Insurance Type</label>
                    <div class="col-sm-6">
                        <select name="id_insurance_type" class="form-select" id="exampleFormControlSelect3"
                            aria-label="Default select example">
                            <option></option>
                            @foreach ($insurance as $insurance)
                                <option value="{{ $insurance->insurance_type_id }}">{{ $insurance->insurance_type_name }}</option>
                            @endforeach
                        </select>
                    </div>
                </div>
                <div class="row justify-content-start mb-3">
                    <label class="col-sm-2 col-form-label text-center" for="basic-default-name">Insurance</label>
                    <div class="col-sm-6">
                        <div class="row">
                            <div class="col-5">
                                <input type="number" name="insurance_issued" class="form-control" id="basic-default-name"
                                    maxlength="4" placeholder="yyyy (from)" />
                            </div>
                            <div class="col-1 text-center">
                                <span class="fw-bold fs-4">-</span>
                            </div>
                            <div class="col-5">
                                <input type="number" name="insurance_expired" class="form-control" id="basic-default-name"
                                    maxlength="4" placeholder="yyyy (until)" />
                            </div>
                        </div>
                    </div>
                </div>
                <div class="row justify-content-start mb-3">
                    <label class="col-sm-2 col-form-label text-center" for="basic-default-name">STNK</label>
                    <div class="col-sm-6">
                        <div class="row">
                            <div class="col-5">
                                <input type="number" name="stnk_issued" class="form-control" id="basic-default-name"
                                    maxlength="4" placeholder="yyyy (from)" />
                            </div>
                            <div class="col-1 text-center">
                                <span class="fw-bold fs-4">-</span>
                            </div>
                            <div class="col-5">
                                <input type="number" name="stnk_expired" class="form-control" id="basic-default-name"
                                    maxlength="4" placeholder="yyyy (until)" />
                            </div>
                        </div>
                    </div>
                </div>
                <div class="row justify-content-start mb-3">
                    <label class="col-sm-2 col-form-label text-center" for="basic-default-name">KIR</label>
                    <div class="col-sm-6">
                        <div class="row">
                            <div class="col-5">
                                <input type="number" name="kir_issued" class="form-control" id="basic-default-name"
                                    maxlength="4" placeholder="yyyy (from)" />
                            </div>
                            <div class="col-1 text-center">
                                <span class="fw-bold fs-4">-</span>
                            </div>
                            <div class="col-5">
                                <input type="number" name="kir_expired" class="form-control" id="basic-default-name"
                                    maxlength="4" placeholder="yyyy (until)" />
                            </div>
                        </div>
                    </div>
                </div>

                <div class="row justify-content-end mt-2 mb-5">
                    <div class="col-sm-10">
                        <button type="submit" class="btn btn-primary">Submit</button>
                    </div>
                </div>
            </form>
        </div>
    </div>

@endsection
