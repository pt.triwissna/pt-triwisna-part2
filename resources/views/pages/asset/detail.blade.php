<div class="modal fade" id="detaildataunit{{ $i->asset_id }}" tabindex="-1" aria-hidden="true">
    <div class="modal-dialog modal-lg" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title" id="exampleModalLabel3">Detail Data Unit</h5>
                <button type="button" class="btn-close" data-bs-dismiss="modal" aria-label="Close"></button>
            </div>
            <hr>
            <div class="modal-body">
                <div class="row mb-3 ms-2">
                    <label class="col-sm-3 col-form-label" for="basic-default-name">No. Unit</label>
                    <div class="col-sm-8">
                        <input type="text" class="form-control" id="basic-default-name" disabled
                            value="{{ $i->no_unit }}" />
                    </div>
                </div>
                <div class="row mb-3 ms-2">
                    <label class="col-sm-3 col-form-label" for="basic-default-name">Type</label>
                    <div class="col-sm-8">
                        <input type="text" class="form-control" id="basic-default-name" disabled
                            value="{{ $i->type_name }}" />
                    </div>
                </div>
                <div class="row mb-3 ms-2">
                    <label class="col-sm-3 col-form-label" for="basic-default-name">Category</label>
                    <div class="col-sm-8">
                        <input type="text" class="form-control" id="basic-default-name" disabled
                            value="{{ $i->ctgr_name }}" />
                    </div>
                </div>
                <div class="row mb-3 ms-2">
                    <label class="col-sm-3 col-form-label" for="basic-default-name">Manufactur</label>
                    <div class="col-sm-8">
                        <input type="text" class="form-control" id="basic-default-name" disabled
                            value="{{ $i->manufactur }}" />
                    </div>
                </div>
                <div class="row mb-3 ms-2">
                    <label class="col-sm-3 col-form-label" for="basic-default-name">Model</label>
                    <div class="col-sm-8">
                        <input type="text" class="form-control" id="basic-default-name" disabled
                            value="{{ $i->model }}" />
                    </div>
                </div>
                <div class="row mb-3 ms-2">
                    <label class="col-sm-3 col-form-label" for="basic-default-name">YOM</label>
                    <div class="col-sm-8">
                        <input type="text" class="form-control" id="basic-default-name" disabled
                            value="{{ $i->yom }}" />
                    </div>
                </div>
                <div class="row mb-3 ms-2">
                    <label class="col-sm-3 col-form-label" for="basic-default-name">Insurance Type</label>
                    <div class="col-sm-8">
                        <input type="text" class="form-control" id="basic-default-name" disabled
                            value="{{ $i->insurance_type_name }}" />
                    </div>
                </div>
                <div class="row mb-3 ms-2">
                    <label class="col-sm-3 col-form-label" for="basic-default-name">Insurance</label>
                    <div class="col-sm-8">
                        <input type="text" class="form-control" id="basic-default-name" disabled
                            value="{{ $i->insurance_issued }} - {{ $i->insurance_expired }}" />
                    </div>
                </div>
                <div class="row mb-3 ms-2">
                    <label class="col-sm-3 col-form-label" for="basic-default-name">STNK</label>
                    <div class="col-sm-8">
                        <input type="text" class="form-control" id="basic-default-name" disabled
                            value="{{ $i->stnk_issued }} - {{ $i->stnk_expired }}" />
                    </div>
                </div>
                <div class="row mb-3 ms-2">
                    <label class="col-sm-3 col-form-label" for="basic-default-name">KIR</label>
                    <div class="col-sm-8">
                        <input type="text" class="form-control" id="basic-default-name" disabled
                            value="{{ $i->kir_issued }} - {{ $i->kir_expired }}" />
                    </div>
                </div>
                <div class="row mb-3 ms-2">
                    <label class="col-sm-3 col-form-label" for="basic-default-name">Created By</label>
                    <div class="col-sm-8">
                        <input type="text" class="form-control" id="basic-default-name" disabled
                            value="{{ $i->username }}" />
                    </div>
                </div>
            </div>
        </div>
    </div>
