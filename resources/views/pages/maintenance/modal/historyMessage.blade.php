<div class="offcanvas offcanvas-end" tabindex="-1" id="revisionhistory" aria-labelledby="offcanvasBackdropLabel">
    <div class="offcanvas-header">
        <h5 id="offcanvasBackdropLabel" class="offcanvas-title">Unrevised revision request data</h5>
        <button type="button" class="btn-close text-reset" data-bs-dismiss="offcanvas" aria-label="Close"></button>
    </div>
    <div class="offcanvas-body mx-0 flex-grow-0">
        @if (count($messageMaintenance) < 1)
            Data Not Found
        @else
            @foreach ($messageMaintenance as $item)
                <div class="card accordion-item active mb-3">
                    <h2 class="accordion-header" id="headingTwo">
                        <button type="button" class="accordion-button collapsed" data-bs-toggle="collapse"
                            data-bs-target="#accordionOne{{ $item->m_maintenance_id }}" aria-expanded="true" aria-controls="accordionTwo">
                            Revision [ {{ $item->created_at }} ] 
                        </button>
                    </h2>

                    <div id="accordionOne{{ $item->m_maintenance_id }}" class="accordion-collapse collapse" data-bs-parent="#accordionExample">
                        <div class="accordion-body">
                            {{ $item->message }}
                        </div>
                    </div>
                </div>
            @endforeach 
        @endif
    </div>
</div>

