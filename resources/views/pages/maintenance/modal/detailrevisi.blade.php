
<div class="modal fade" id="m_maintenace{{ $item->maintenance_id }}" tabindex="-1" aria-hidden="true">
    <div class="modal-dialog modal-lg" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title" id="exampleModalLabel3">Detailed maintenance data revised</h5>
                <button type="button" class="btn-close" data-bs-dismiss="modal" aria-label="Close"></button>
            </div>
            <hr>
            <div class="modal-body">
                <form>
                    <div class="row mb-3 ms-2">
                        <label class="col-sm-3 col-form-label" for="basic-default-name">Start Breakdown</label>
                        <div class="col-sm-8">
                            <input type="text" class="form-control" id="basic-default-name" disabled value="{{$item->s_breakdown_date}}, {{$item->s_breakdown_time}}"/>
                        </div>
                    </div>
                    <div class="row mb-3 ms-2">
                        <label class="col-sm-3 col-form-label" for="basic-default-name">Finish Breakdown</label>
                        <div class="col-sm-8">
                            <input type="text" class="form-control" id="basic-default-name" disabled value="{{$item->f_breakdown_date}}, {{$item->f_breakdown_time}}"/>
                        </div>
                    </div>
                    <div class="row mb-3 ms-2">
                        <label class="col-sm-3 col-form-label" for="basic-default-name">Issue</label>
                        <div class="col-sm-8">
                            <input type="text" class="form-control" id="basic-default-name" disabled value="{{$item->issue}}"/>
                        </div>
                    </div>
                    <div class="row mb-3 ms-2">
                        <label class="col-sm-3 col-form-label" for="basic-default-name">Perform By</label>
                        <div class="col-sm-8">
                            <input type="text" class="form-control" id="basic-default-name" disabled value="{{$item->perform_by}}"/>
                        </div>
                    </div>
                    <div class="row mb-3 ms-2">
                        <label class="col-sm-3 col-form-label" for="basic-default-name">Finance</label>
                        <div class="col-sm-8">
                            <input type="text" class="form-control" id="basic-default-name" disabled value="Rp. {{ number_format($item->finance, 0, ',', '.') }}"/>
                        </div>
                    </div>
                    
                    <div class="row mb-3 ms-2">
                        <label class="col-sm-3 col-form-label" for="basic-default-name">Unit Number</label>
                        <div class="col-sm-8">
                            <input type="text" class="form-control" id="basic-default-name" disabled value="{{$item->no_unit}}"/>
                        </div>
                    </div>
                    <div class="row mb-3 ms-2">
                        <label class="col-sm-3 col-form-label" for="basic-default-name">Manufacture</label>
                        <div class="col-sm-8">
                            <input type="text" class="form-control" id="basic-default-name" disabled value="{{$item->manufactur}}"/>
                        </div>
                    </div>
                    <div class="row mb-3 ms-2">
                        <label class="col-sm-3 col-form-label" for="basic-default-name">Model</label>
                        <div class="col-sm-8">
                            <input type="text" class="form-control" id="basic-default-name" disabled value="{{$item->model}}"/>
                        </div>
                    </div>
                </form>
            </div>
        </div>
    </div>
</div>
