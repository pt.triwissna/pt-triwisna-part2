<div class="modal modal-top fade" id="message{{ $item->maintenance_id }}" tabindex="-1" aria-hidden="true">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title" id="exampleModalLabel3">Revision Request form to admin</h5>
                <button type="button" class="btn-close" data-bs-dismiss="modal" aria-label="Close"></button>
            </div>
            <hr>
            <div class="modal-body">
                <form action="{{ route('messageMaintenance', ['id' => $item->maintenance_id]) }}" method="POST">
                    @csrf
                    <div class="row">
                        <div class="col mb-3">
                            <label for="nameSlideTop" class="form-label">MESSAGE</label>
                            <textarea id="basic-default-message" name="message" class="form-control" placeholder="Revised Specifications" style="height: 100px"></textarea>
                        </div>
                    </div>
                    <div class="modal-footer">
                        <button type="submit" class="btn btn-primary">Submit</button>
                    </div>
                </form>
            </div>
        </div>
    </div>
</div>
