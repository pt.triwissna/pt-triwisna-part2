@extends('app')
@section('title-app')
    Maintenance
@endsection
@section('navbar-title-back')
    Maintenance Record
@endsection
@section('content')

    @if (session('success'))
        @include('partials.alert-success')
    @endif

    @if (session('error'))
        @include('partials.alert-error')
    @endif
    <div class="card p-3">
        <div class="d-grid gap-2 d-md-flex justify-content-md-end">
            @if ($loggedInUser->role->role == 'admin' || $loggedInUser->role->role == 'manager')
                <a href="{{ route('add-maintenance') }}" class="btn btn-primary me-md-2 pe-5 ps-5">Add Record</a>
            @endif
            @if ($loggedInUser->role->role == 'supervisor')
                {{-- <button class="btn btn-primary" type="button" data-bs-toggle="offcanvas" data-bs-target="#revisionrequest"
                    aria-controls="offcanvasBackdrop"><i class='bx bx-history'></i>
                    Revision Request <span class="badge rounded-pill badge-center h-px-20 w-px-20 bg-danger">3</span>
                </button> --}}
                <button class="btn btn-primary" type="button" data-bs-toggle="offcanvas" data-bs-target="#revisionhistory"
                    aria-controls="offcanvasBackdrop"><i class='bx bx-history'></i>
                    Revision
                </button>
                @include('pages.maintenance.modal.historyMessage')
            @endif
        </div>
        <form action="{{ route('maintenance') }}" method="get">
            @csrf
            @if ($loggedInUser->role->role != 'admin')
                <div class="row mb-5 mt-5">
                @else
                    <div class="row mb-5">
            @endif
            <div class="col-md-2">
                <label for="" class="fw-bold">Start Breakdown Time</label>
                <input name="s_breakdown_date" type="date" class="form-control"
                    value="{{ isset($_GET['s_breakdown_date']) ? $_GET['s_breakdown_date'] : '' }}">
            </div>
            <div class="col-md-2">
                <label for="" class="fw-bold">Finish Breakdown Time</label>
                <input name="f_breakdown_date" type="date" class="form-control"
                    value="{{ isset($_GET['f_breakdown_date']) ? $_GET['f_breakdown_date'] : '' }}">
            </div>
            <div class="col-md-2">
                <label for="" class="fw-bold">No Asset</label>
                <input name="no_unit" type="text" class="form-control" placeholder="No. Asset"
                    value="{{ isset($_GET['no_unit']) ? $_GET['no_unit'] : '' }}">
            </div>
            <div class="col-md-2">
                <button type="submit" class="btn btn-primary mt-4">Search</button>
            </div>
    </div>
    </form>

    <div class="table-responsive text-nowrap">
        <table class="table table-striped table-hover">
            <thead>
                <tr class="table-active">
                    <th class="fw-bold">Action</th>
                    <th class="fw-bold">Start Breakdown</th>
                    <th class="fw-bold">Finish Breakdown</th>
                    <th class="fw-bold">Asset</th>
                    <th class="fw-bold">Issue</th>
                    <th class="fw-bold">Perform By</th>
                    <th class="fw-bold">Expense</th>
                </tr>
            </thead>
            <tbody class="table-border-bottom-0">
                @if (count($maintenance) < 1)
                    <tr>
                        <td colspan="8" style="padding: 20px; font-size: 20px;"><span>No data found</span>
                        </td>
                    </tr>
                @else
                    @foreach ($maintenance as $key => $item)
                        @include('pages.maintenance.modal.detail_modal')
                        @include('pages.maintenance.modal.message_modal')
                        <tr>
                            <td>
                                <button class="btn btn-primary btn-icon btm-sm" data-bs-toggle="modal"
                                    data-bs-target="#maintenance{{ $item->maintenance_id }}"><i
                                        class='bx bx-detail'></i></button>
                                @if ($loggedInUser->role->role == 'supervisor')
                                    <a class="btn btn-icon btn-warning btm-sm"
                                        href="{{ route('pageEditMaintenance', ['id' => $item->maintenance_id]) }}"><i
                                            class='bx bxs-edit'></i></a>
                                    @if ($item->flg == 'N')
                                        <button class="btn btn-icon btn-success btm-sm" data-bs-toggle="modal"
                                            data-bs-target="#message{{ $item->maintenance_id }}"><i
                                                class='bx bx-message-alt-edit'></i></button>
                                    @endif
                                @endif
                                @if ($loggedInUser->role->role == 'manager')
                                    <a class="btn btn-icon btn-warning btm-sm"
                                        href="{{ route('pageEditMaintenance', ['id' => $item->maintenance_id]) }}"><i
                                            class='bx bxs-edit'></i></a>
                                    <button class="btn btn-danger btn-icon btm-sm" data-bs-toggle="modal"
                                        data-bs-target="#delete{{ $item->maintenance_id }}"><i class='bx bxs-trash'></i></button>
                                        @include('pages.maintenance.modal.confirm')
                                @endif
                            </td>
                            <td>{{ $item->s_breakdown_date }}, {{ $item->s_breakdown_time }}</td>
                            <td>{{ $item->f_breakdown_date }}, {{ $item->f_breakdown_time }}</td>
                            <td>{{ $item->no_unit }}, {{ $item->manufactur }}</td>
                            <td>{{ $item->issue }}</td>
                            <td>{{ $item->perform_by }}</td>
                            <td>Rp. {{ number_format($item->finance, 0, ',', '.') }}</td>
                        </tr>
                    @endforeach
                @endif
            </tbody>
        </table>
    </div>
    <div class="row pt-5">
        <div class="col-lg-10">
            <ul class="pagination">
                {{ $maintenance->links() }}
            </ul>
            <br>
            <span>Total data {{ $total[0]->totalData }}, pages {{ $maintenance->currentPage() }} of
                {{ $maintenance->lastPage() }}</span>
        </div>
    </div>
    </div>
@endsection
