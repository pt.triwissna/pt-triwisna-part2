@extends('app')
@section('title-app')
    Revisi Maintenance record
@endsection
@section('navbar-title-back')
    Revisi Maintenance
@endsection
@section('navbar-title-target')
    / Revisi
@endsection
@section('link-back')
    {{ route('revisiPage.maintenance') }}
@endsection
@section('content')
    <div class="card p-3">
        <div class="container-view">
            <a href="{{ route('revisiPage.maintenance') }}">
                <button type="button" class="btn btn-primary">Back</button>
            </a>
            <hr>
            <div class="card bg-primary text-white mb-3">
                <div class="card-body">
                    <p class="card-text" style="white-space: pre-line;">{{ $m_revisi->message }}</p>
                </div>
            </div>
            <form action="{{ route('revisiMaintenance.action', ['id' => $maintenance->maintenance_id, 'param' => $param]) }}" method="post">
                @csrf
                <div class="row justify-content-start mb-3">
                    <label class="col-sm-3 col-form-label text-center" for="basic-default-name">Start Breakdown Date</label>
                    <div class="col-sm-6">
                        <input type="date" name="s_breakdown_date"
                            class="form-control @error('issue') is-invalid @enderror" id="basic-default-name"
                            value="{{ old('s_breakdown_date', $maintenance->s_breakdown_date) }}">
                        @error('s_breakdown_date')
                            <div class="invalid-feedback">
                                {{ $message }}
                            </div>
                        @enderror
                    </div>
                </div>
                <div class="row justify-content-start mb-3">
                    <label class="col-sm-3 col-form-label text-center" for="basic-default-name">Start Breakdown Time</label>
                    <div class="col-sm-6">
                        <input type="time" name="s_breakdown_time"
                            class="form-control @error('issue') is-invalid @enderror" id="basic-default-name"
                            value="{{ old('s_breakdown_time', $maintenance->s_breakdown_time) }}">
                        @error('s_breakdown_time')
                            <div class="invalid-feedback">
                                {{ $message }}
                            </div>
                        @enderror
                    </div>
                </div>
                <div class="row justify-content-start mb-3">
                    <label class="col-sm-3 col-form-label text-center" for="basic-default-name">Finish Breakdown
                        Date</label>
                    <div class="col-sm-6">
                        <input type="date" name="f_breakdown_date"
                            class="form-control @error('issue') is-invalid @enderror" id="basic-default-name"
                            value="{{ old('f_breakdown_date', $maintenance->f_breakdown_date) }}">
                        @error('f_breakdown_date')
                            <div class="invalid-feedback">
                                {{ $message }}
                            </div>
                        @enderror
                    </div>
                </div>
                <div class="row justify-content-start mb-3">
                    <label class="col-sm-3 col-form-label text-center" for="basic-default-name">Finish Breakdown
                        Time</label>
                    <div class="col-sm-6">
                        <input type="time" name="f_breakdown_time"
                            class="form-control @error('issue') is-invalid @enderror" id="basic-default-name"
                            value="{{ old('f_breakdown_time', $maintenance->f_breakdown_time) }}">
                        @error('f_breakdown_time')
                            <div class="invalid-feedback">
                                {{ $message }}
                            </div>
                        @enderror
                    </div>
                </div>
                <div class="row justify-content-start mb-3">
                    <label class="col-sm-3 col-form-label text-center" for="basic-default-name">Asset</label>
                    <div class="col-sm-6">
                        <select name="asset_id" class="form-select" id="exampleFormControlSelect1"
                            aria-label="Default select example">
                            <option value=""></option>
                            @foreach ($asset as $i)
                                <option value="{{ $i->asset_id }}"
                                    {{ (old('asset_id') ?? $maintenance->asset_id) == $i->asset_id ? 'selected' : '' }}>
                                    {{ $i->no_unit }}, {{ $i->manufactur }}</option>
                            @endforeach
                        </select>
                        @error('asset_id')
                            <div class="invalid-feedback">
                                {{ $message }}
                            </div>
                        @enderror
                    </div>
                </div>
                <div class="row justify-content-start mb-3">
                    <label class="col-sm-3 col-form-label text-center" for="basic-default-name">Issue</label>
                    <div class="col-sm-6">
                        <input type="text" name="issue" class="form-control @error('issue') is-invalid @enderror"
                            id="basic-default-name" value="{{ old('issue', $maintenance->issue) }}">
                        @error('issue')
                            <div class="invalid-feedback">
                                {{ $message }}
                            </div>
                        @enderror
                    </div>
                </div>
                <div class="row justify-content-start mb-3">
                    <label class="col-sm-3 col-form-label text-center" for="basic-default-name">Perform By</label>
                    <div class="col-sm-6">
                        <input type="text" name="perform_by"
                            class="form-control @error('perform_by') is-invalid @enderror" id="basic-default-name"
                            value="{{ old('perform_by', $maintenance->perform_by) }}">
                        @error('perform_by')
                            <div class="invalid-feedback">
                                {{ $message }}
                            </div>
                        @enderror
                    </div>
                </div>

                <div class="row justify-content-start mb-3">
                    <label class="col-sm-3 col-form-label text-center" for="basic-default-name">Finance</label>
                    <div class="col-sm-6">
                        <input type="number" name="finance" class="form-control @error('finance') is-invalid @enderror"
                            id="basic-default-name" value="{{ old('finance', $maintenance->finance) }}">
                        @error('finance')
                            <div class="invalid-feedback">
                                {{ $message }}
                            </div>
                        @enderror
                    </div>
                </div>
                <div class="row justify-content-start mb-5">
                    <div class="col-sm-9 offset-sm-3">
                        <button type="submit" class="btn btn-primary">Submit</button>
                    </div>
                </div>
            </form>
        </div>
    </div>
@endsection
