@extends('app')
@section('title-app')
    Revisi Maintenance
@endsection
@section('navbar-title-back')
    Revisi Maintenance
@endsection
@section('content')
    @if (session('success'))
        @include('partials.alert-success')
    @endif

    @if (session('error'))
        @include('partials.alert-error')
    @endif
    <div class="card p-3">
        <div class="d-grid gap-2 d-md-flex justify-content-md-end">

        </div>
        <div class="table-responsive text-nowrap">
            <table class="table table-striped table-hover">
                <thead>
                    <tr class="table-active">
                        <th class="fw-bold">Action</th>
                        <th class="fw-bold">Date</th>
                        <th class="fw-bold">Message</th>
                    </tr>
                </thead>
                <tbody class="table-border-bottom-0">
                    @if (count($m_maintenance) < 1)
                        <tr>
                            <td colspan="10" style="padding: 20px; font-size: 20px;"><span>No Revision Requests</span>
                            </td>
                        </tr>
                    @else
                        @foreach ($m_maintenance as $item)
                        @include('pages.maintenance.modal.detailrevisi')
                            <tr>
                                <td>
                                    <button class="btn btn-primary btn-icon btm-sm" data-bs-toggle="modal"
                                        data-bs-target="#m_maintenace{{ $item->m_maintenance_id  }}"><i
                                            class='bx bx-detail'></i></button>
                                    <a href="{{ route('revisiMaintenance.page', ['id' => $item->m_maintenance_id ]) }}"
                                        class="btn btn-success btn-icon btm-sm"><i class='bx bx-check'
                                            style='font-size: 2.2em; font-weight: bold'></i></a>
                                </td>
                                <td>{{ $item->date }}</td>
                                <td style="white-space: pre-line;">{{ $item->message }}</td>
                            </tr>
                        @endforeach
                    @endif
                </tbody>
            </table>
        </div>

    </div>
@endsection



