@extends('app')
@section('title-app')
Add Maintenance record
@endsection
@section('navbar-title-back')
Maintenance Record
@endsection
@section('navbar-title-target')
/ Add Record
@endsection
@section('link-back')
{{ route('maintenance') }}
@endsection
@section('content')
<div class="card p-3">
    <div class="container-view">
        <a href="{{ route('maintenance') }}">
            <button type="button" class="btn btn-primary">Back</button>
        </a>
        <hr>
        <br>
        <form action="{{route('add-maintenance-record')}}" method="post">
            @csrf
            <div class="row mb-3 justify-content-start">
                <label class="col-sm-3 col-form-label text-center" for="basic-default-name">Start Breakdown</label>
                <div class="col-sm-6">
                    <input type="datetime-local" name="s_breakdown" 
                        class="form-control @error('s_breakdown') is-invalid @enderror" id="basic-default-name" value="{{old('s_breakdown')}}">
                    @error('s_breakdown')
                        <div class="invalid-feedback">
                            {{$message}}
                        </div>
                    @enderror
                </div>
            </div>
            <div class="row mb-3 justify-content-start">
                <label class="col-sm-3 col-form-label text-center" for="basic-default-name">Finish Breakdown</label>
                <div class="col-sm-6">
                    <input type="datetime-local" name="f_breakdown" 
                        class="form-control @error('f_breakdown') is-invalid @enderror" id="basic-default-name" value="{{old('f_breakdown')}}">
                        @error('f_breakdown')
                        <div class="invalid-feedback">
                            {{$message}}
                        </div>
                    @enderror
                </div>
            </div>
            <div class="row justify-content-start mb-3">
                <label class="col-sm-3 col-form-label text-center" for="basic-default-name">Asset</label>
                <div class="col-sm-6">
                    <select name="asset_id" class="form-select" id="exampleFormControlSelect1" aria-label="Default select example">
                        <option value=""></option>
                        @foreach ($asset as $i)
                            <option value="{{ $i->asset_id }}" {{ old('asset_id') == $i->asset_id ? 'selected' : '' }}>{{ $i->no_unit }}, {{$i->manufactur}}</option>
                        @endforeach
                    </select>
                    @error('asset_id')
                    <div class="invalid-feedback">
                        {{ $message }}
                    </div>
                    @enderror
                </div>
            </div>
            <div class="row justify-content-start mb-3">
                <label class="col-sm-3 col-form-label text-center" for="basic-default-name">Issue</label>
                <div class="col-sm-6">
                    <input type="text" name="issue" class="form-control @error('issue') is-invalid @enderror" id="basic-default-name" value="{{ old('issue') }}">
                    @error('issue')
                    <div class="invalid-feedback">
                        {{ $message }}
                    </div>
                    @enderror
                </div>
            </div>
            <div class="row justify-content-start mb-3">
                <label class="col-sm-3 col-form-label text-center" for="basic-default-name">Perform By</label>
                <div class="col-sm-6">
                    <input type="text" name="perform_by" class="form-control @error('perform_by') is-invalid @enderror" id="basic-default-name" value="{{ old('perform_by') }}">
                    @error('perform_by')
                    <div class="invalid-feedback">
                        {{ $message }}
                    </div>
                    @enderror
                </div>
            </div>
            
            <div class="row justify-content-start mb-3">
                <label class="col-sm-3 col-form-label text-center" for="basic-default-name">Finance</label>
                <div class="col-sm-6">
                    <input type="number" name="finance" class="form-control @error('finance') is-invalid @enderror" id="basic-default-name" value="{{ old('finance') }}">
                    @error('finance')
                    <div class="invalid-feedback">
                        {{ $message }}
                    </div>
                    @enderror
                </div>
            </div>
            <div class="row justify-content-start mb-5">
                <div class="col-sm-9 offset-sm-3">
                    <button type="submit" class="btn btn-primary">Submit</button>
                </div>
            </div>
        </form>
    </div>
</div>
@endsection
