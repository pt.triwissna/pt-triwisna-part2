

<!DOCTYPE html>
<html>

<head>
    <style>
        body {
            font-family: Arial, sans-serif;
        }

        h5,
        h6 {
            margin: 0;
            font-weight: bold;
        }

        h6 {
            margin-top: 10px;
        }

        .text-center {
            text-align: center;
        }

        .container {
            width: 80%;
            margin: 0 auto;
        }

        .row::after {
            content: "";
            clear: both;
            display: table;
        }

        .col-4 {
            width: 40%;
            float: left;
        }

        .col-3 {
            width: 20%;
            float: left;
        }


        table {
            width: 100%;
            border-collapse: collapse;
        }

        th,
        td {
            border: 1px solid #000;
            text-align: center;
            padding: 5px;
        }

        th.centered {
            vertical-align: middle;
        }

        .fw-bold {
            font-weight: bold;
        }

        .font-size {
            font-size: 12px;
        }

        .size-req {
            font-size: 14px;
        }


    </style>
</head>

<body>
    <div class="container">
        <h2 class="text-center">Report Maintenance Record</h2>
        <div class="row">
            <div class="col-3">
                <p class="size-req">Start Breakdown</p>
            </div>
            <div class="col-4">
                <p class="size-req">: {{ $req->s_breakdown_from }} - {{ $req->s_breakdown_to }}</p>
            </div>
        </div>
        <div class="row">
            <div class="col-3">
                <p class="size-req">Finish Breakdown</p>
            </div>
            <div class="col-4">
                <p class="size-req">: {{ $req->f_breakdown_from }} - {{ $req->f_breakdown_to }}</p>
            </div>
        </div>
        <div class="row">
            <div class="col-3">
                <p class="size-req">Asset</p>
            </div>
            <div class="col-4">
                <p class="size-req">: {{ $req->asset }}</p>
            </div>
        </div>
        <div class="row">
            <div class="col-3">
                <p class="size-req">Printed on</p>
            </div>
            <div class="col-4">
                <p class="size-req">: {{ $req->print_on }}</p>
            </div>
        </div>

        <br>
        <div class="row mt-5">
            <div class="col-8">
                <table>
                    <thead>
                        <tr style="background-color: #d3d3d3">
                            <th rowspan="3" class="centered fw-bold">NO</th>
                            <th colspan="4" class="text-center fw-bold">Breakdown time</th>
                            <th rowspan="3" class="centered fw-bold">Asset</th>
                            <th rowspan="3" class="centered fw-bold">Issue</th>
                            <th rowspan="3" class="centered fw-bold">Perform By</th>
                            <th rowspan="3" class="centered fw-bold">Expense</th>
                        </tr>
                        <tr style="background-color: #d3d3d3">
                            <th colspan="2" class="text-center">Start</th>
                            <th colspan="2" class="text-center">Finish</th>
                        </tr>
                        <tr style="background-color: #d3d3d3">
                            <th class="text-center">Date</th>
                            <th class="text-center">Time</th>
                            <th class="text-center">Date</th>
                            <th class="text-center">Time</th>
                        </tr>
                    </thead>
                    <tbody>
                        @php
                            $no = 1;
                        @endphp
                        @foreach ($data as $item)
                            <tr>
                                <td class="font-size">{{ $no++ }}</td>
                                <td class="font-size">{{ $item->s_breakdown_date }}</td>
                                <td class="font-size">{{ $item->s_breakdown_time }}</td>
                                <td class="font-size">{{ $item->f_breakdown_date }}</td>
                                <td class="font-size">{{ $item->f_breakdown_time }}</td>
                                <td class="font-size">{{ $item->no_unit }}, {{ $item->manufactur }}</td>
                                <td class="font-size">{{ $item->issue }}</td>
                                <td class="font-size">{{ $item->perform_by }}</td>
                                <td class="font-size">Rp. {{ number_format($item->finance, 0, ',', '.') }}</td>
                            </tr>
                        @endforeach

                    </tbody>
                </table>
            </div>
        </div>
    </div>
</body>

</html>
