@extends('app')
@section('title-app')
    Report Maintenance
@endsection
@section('navbar-title-back')
    Report Maintenance Record
@endsection
@section('content')
    <div class="card p-3">
        <form action="{{ route('downloadReport') }}">
            @csrf
            <div class="row mt-5 justify-content-start">
                <label class="col-sm-3 col-form-label text-center" for="basic-default-name">Start Breakdown</label>
                <div class="col-sm-3">
                    <input type="date" name="s_breakdown_from" class="form-control" id="basic-default-name">
                </div>
                <div class="col-sm-1 text-center" style="line-height: 38px;">
                    to
                </div>
                <div class="col-sm-3">
                    <input type="date" name="s_breakdown_to" class="form-control" id="basic-default-name">
                </div>
            </div>
            <div class="row mt-4 justify-content-start">
                <label class="col-sm-3 col-form-label text-center" for="basic-default-name">Finish Breakdown</label>
                <div class="col-sm-3">
                    <input type="date" name="f_breakdown_from" class="form-control" id="basic-default-name">
                </div>
                <div class="col-sm-1 text-center" style="line-height: 38px;">
                    to
                </div>
                <div class="col-sm-3">
                    <input type="date" name="f_breakdown_to" class="form-control" id="basic-default-name">
                </div>
            </div>
            <div class="row mt-4 justify-content-start">
                <label class="col-sm-3 col-form-label text-center" for="basic-default-name">Asset</label>
                <div class="col-sm-7">
                    <select name="asset_id" class="form-select" id="exampleFormControlSelect1" aria-label="Default select example">
                        <option value=""></option>
                        @foreach ($asset as $i)
                            <option value="{{ $i->asset_id }}" {{ old('asset_id') == $i->asset_id ? 'selected' : '' }}>{{ $i->no_unit }}, {{$i->manufactur}}</option>
                        @endforeach
                    </select>
                </div>
            </div>
            <div class="row mt-5 mb-3">
                <div class="col-sm-10 text-end">
                    <button type="submit" class="btn btn-primary">Download Report</button>
                </div>
            </div>
        </form>
    </div>
@endsection
