<style>
    /* Animasi garis loading */
    .loading-bar {
        height: 7px;
        background-color: #FF3E1D; /* Ganti dengan warna yang Anda inginkan */
        animation: loading 3s linear infinite; /* 3s mengacu pada waktu alert muncul */
        margin-top: 0px;
        margin-bottom: 10px;
        margin-left: 2px;
    }

    @keyframes loading {
        0% { width: 100%; }
        100% { width: 0%; }
    }

    @keyframes zoomIn {
        from {
            transform: scale(0);
            opacity: 0;
        }
        to {
            transform: scale(1);
            opacity: 1;
        }
    }

    .alert-zoom-in {
        animation: zoomIn 0.3s ease forwards;
    }

    @keyframes slideOutUp {
        from {
            transform: translateY(0);
            opacity: 1;
        }
        to {
            transform: translateY(-100%);
            opacity: 0;
        }
    }

    .alert-slide-up {
        animation: slideOutUp 0.3s ease forwards;
    }
</style>

<!-- Kode untuk menampilkan alert error -->
@if(session('error'))
<div class="alert alert-danger alert-zoom-in" style="width: 100%; height: 50px; display: flex; align-items: center; justify-content: center; margin-bottom: 0; padding: 0px;">
    {{ session('error') }}
</div>
<div class="loading-bar"></div>
@endif

@if($errors->any())
<div class="alert alert-danger alert-zoom-in" style="width: 100%; height: 50px; display: flex; align-items: center; justify-content: center; margin-bottom: 0; padding: 0px;">
    @foreach ($errors->all() as $error)
    {{ $error }}
    @endforeach
</div>
<div class="loading-bar"></div>
@endif
<script>
    // Mengatur timeout 3 detik sejak tampilan halaman dimuat
    setTimeout(function() {
        // Menambahkan class untuk memulai animasi slide out
        var alertElement = document.querySelector('.alert');
        if (alertElement) {
            alertElement.classList.add('alert-slide-up');
            document.querySelector('.loading-bar').style.display = 'none';

            // Menghapus elemen setelah animasi selesai
            setTimeout(function() {
                alertElement.remove();
            }, 300); // 300ms = durasi animasi
        }
    }, 3000); // 3000ms = 3 detik
</script>
