<aside id="layout-menu" class="layout-menu menu-vertical menu bg-menu-theme">
    <div class="app-brand-demo">
        <a href="#" class="app-brand-link">
            <div class="app-brand-image-container">
                <img src="{{ asset('img/logo.png') }}" alt="Logo Sneat" class="app-brand-image">
            </div>
        </a>
        <span class="app-brand-text demo">PT Triwisnna</span>
        {{-- class logo --}}
        <a href="javascript:void(0);" class="layout-menu-toggle menu-link text-large ms-auto d-block d-xl-none">
            <i class="bx bx-chevron-left bx-sm align-middle"></i>
        </a>
    </div>
    <div class="menu-inner-shadow"></div>

    <ul class="menu-inner py2">
        <!-- Dashboard -->
        <li class="menu-item {{ request()->routeIs('home') ? 'active' : '' }}">
            <a href="/" class="menu-link">
                <i class="menu-icon tf-icons bx bx-home-circle"></i>
                <div data-i18n="Analytics">Home</div>
            </a>
        </li>


        <li class="menu-header small text-uppercase">
            <span class="menu-header-text">Pages</span>
        </li>

        <li
            class="menu-item {{ request()->routeIs('index.operations', 'create.operations', 'report-operation', 'revisiPage.operation') ? 'active open' : '' }}">
            <a href="javascript:void(0);" class="menu-link menu-toggle">
                <i class="menu-icon tf-icons bx bx-detail"></i>
                <div data-i18n="operation">Operations</div>
            </a>
            <ul class="menu-sub">
                <li
                    class="menu-item {{ request()->routeIs('index.operations', 'create.operations') || request()->routeIs('create') ? 'active' : '' }}">
                    <a href="{{ route('index.operations') }}" class="menu-link">
                        <div data-i18n="peminjaman">Operation</div>
                    </a>
                </li>
                @if ($loggedInUser->role->role == 'admin')
                    <li class="menu-item {{ request()->routeIs('revisiPage.operation') ? 'active' : '' }}">
                        <a href="{{ route('revisiPage.operation') }}" class="menu-link">
                            <div data-i18n="peminjaman">Request Revision</div>
                        </a>
                    </li>
                @endif
                <li class="menu-item {{ request()->routeIs('report-operation') ? 'active' : '' }}">
                    <a href="{{ route('report-operation') }}" class="menu-link">
                        <div data-i18n="operation">Report Operation Record</div>
                    </a>
                </li>
            </ul>
        </li>
        <li
            class="menu-item {{ request()->routeIs('maintenance', 'add-maintenance', 'report-maintenance', 'revisiPage.maintenance', 'revisiMaintenance.page') ? 'active open' : '' }}">
            <a href="javascript:void(0);" class="menu-link menu-toggle">
                <i class="menu-icon tf-icons bx bx-detail"></i>
                <div data-i18n="maintenance">Maintenance</div>
            </a>
            <ul class="menu-sub">
                <li class="menu-item {{ request()->routeIs('maintenance', 'add-maintenance') ? 'active' : '' }}">
                    <a href="{{ route('maintenance') }}" class="menu-link">
                        <div data-i18n="maintenance-record">Maintenance Record</div>
                    </a>
                </li>
                @if ($loggedInUser->role->role == 'admin')
                    <li class="menu-item {{ request()->routeIs('revisiPage.maintenance', 'revisiMaintenance.page') ? 'active' : '' }}">
                        <a href="{{ route('revisiPage.maintenance') }}" class="menu-link">
                            <div data-i18n="maintenance">Request Revision</div>
                        </a>
                    </li>
                @endif
                <li class="menu-item {{ request()->routeIs('report-maintenance') ? 'active' : '' }}">
                    <a href="{{ route('report-maintenance') }}" class="menu-link">
                        <div data-i18n="riwayat-peminjaman">Report Maintenance Record</div>
                    </a>
                </li>
            </ul>
        </li>
        <li
            class="menu-item {{ request()->routeIs('index.asset') || request()->routeIs('create.asset') ? 'active' : '' }}">
            <a href="{{ route('index.asset') }}" class="menu-link">
                <i class="menu-icon tf-icons bx bx-table"></i>
                <div data-i18n="data-unit">Data Asset</div>
            </a>
        </li>
    </ul>
</aside>
