<?php

namespace App\Rules;

use Closure;
use Illuminate\Contracts\Validation\ValidationRule;

class OdometerAndHourmeter implements ValidationRule
{
    /**
     * Run the validation rule.
     *
     * @param  \Closure(string): \Illuminate\Translation\PotentiallyTranslatedString  $fail
     */
    public function validate(string $attribute, mixed $value, Closure $fail): void
    {
        $otherAttribute = str_replace(['s_', 'f_'], '', $attribute);
        $otherValue = request()->input("f_$otherAttribute");

        if ($value >= $otherValue) {
            $fail("Nilai $attribute harus lebih kecil dari $otherAttribute.");
        } elseif ($otherValue <= $value) {
            $fail("Nilai $otherAttribute harus lebih besar dari $attribute.");
        }
    }

    public function message()
    {
        return 'Nilai :attribute harus lebih kecil dari :other_attribute.';
    }
}
