<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class typeInsuranceModels extends Model
{
    protected $table = 'insurance_type';
    protected $primaryKey = 'insurance_type_id';
    protected $fillable = ['insurance_type_name'];

    protected $guarded = ['insurance_type_id', 'created_at', 'updated_at'];
}
