<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class MessageMaintenanceModel extends Model
{
    protected $table = 'm_maintenance';
    protected $primaryKey = 'm_maintenance_id';

    protected $guarded = [];
}
