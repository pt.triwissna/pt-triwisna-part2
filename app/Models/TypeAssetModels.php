<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class TypeAssetModels extends Model
{
    protected $table = 'type_asset';
    protected $primaryKey = 'type_asset_id';

    protected $guarded = ['type_asset_id', 'created_at', 'updated_at'];
    protected $fillable = ['type_name'];

    public function assets()
    {
        return $this->hasMany(assetModels::class, 'type_asset_id');
    }
}
