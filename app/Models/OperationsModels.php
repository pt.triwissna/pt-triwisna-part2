<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class OperationsModels extends Model
{
    protected $table = 'operation';

    protected $primaryKey = 'operation_id';

    protected $fillable = [ 'record_id','date', 'shift', 'project', 'operator', 'asset_id', 'fuel', 'trip', 's_worktime', 'f_worktime', 's_odometer', 'f_odometer', 's_hourmeter', 'f_hourmeter', 's_otherdelay', 'f_otherdelay', 'flg'];

    protected $guarded = ['operation_id', 'created_at', 'updated_at'];

    public function unit()
    {
        return $this->belongsTo(AssetModels::class, 'asset_id');
    }
}
