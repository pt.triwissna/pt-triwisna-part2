<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class permitsModels extends Model
{
    protected $table = 'permit_insurance';
    protected $primaryKey = 'permit_insurance_id';
    protected $fillable = ['stnk_issued', 'stnk_expired', 'kir_issued', 'kir_expired', 'insurance_type_id', 'insurance_issued', 'insurance_expired'];

    protected $guarded = ['permit_insurance_id', 'created_at', 'updated_at'];
}
