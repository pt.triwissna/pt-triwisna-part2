<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class MaintenanceModel extends Model
{
    protected $table = 'maintenance';
    protected $primaryKey = 'maintenance_id';

    protected $fillable = [ 'record_id', 'asset_id', 's_breakdown_date', 's_breakdown_time', 'f_breakdown_date', 'f_breakdown_time', 'issue', 'perform_by', 'finance', 'flg' ];

    protected $guarded = ['maintenance_id'];
}
