<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class CtgrAssetModels extends Model
{
    protected $table = 'ctgr_asset';
    protected $fillable = ['ctgr_name'];
    protected $primaryKey = 'ctgr_asset_id';

    protected $guarded = ['ctgr_asset_id', 'created_at', 'updated_at'];

    public function assets()
    {
        return $this->hasMany(assetModels::class, 'ctgr_asset_id');
    }
}
