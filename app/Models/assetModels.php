<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class assetModels extends Model
{
    protected $table = 'asset';

    protected $primaryKey = 'asset_id';

    protected $guarded = ['asset_id', 'created_at', 'updated_at'];
    protected $fillable = ['record_id', 'type_asset_id', 'ctgr_asset_id', 'manufactur', 'model', 'yom', 'no_unit', 'permit_insurance_id'];

    public function typeAsset()
    {
        return $this->belongsTo(TypeAssetModels::class, 'type_asset_id');
    }

    public function ctgrAsset()
    {
        return $this->belongsTo(CtgrAssetModels::class, 'ctgr_asset_id');
    }

    public function permitInsurance()
    {
        return $this->belongsTo(permitsModels::class, 'permit_insurance_id');
    }

    public function user()
    {
        return $this->belongsTo(User::class, 'record_id');
    }
}
