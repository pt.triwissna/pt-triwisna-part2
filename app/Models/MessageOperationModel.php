<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class MessageOperationModel extends Model
{
    protected $table = 'm_operation';
    protected $primaryKey = 'm_operation_id';

    protected $guarded = [];
}
