<?php

namespace App\Http\Controllers;

use \Barryvdh\DomPDF\Facade\PDF;
use Carbon\Carbon;
use DateTime;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
date_default_timezone_set("Asia/Jakarta");

class ReportMaintenanceController extends Controller
{
    public function index()
    {
        $query = "
            SELECT asset_id, no_unit, manufactur, model 
            FROM asset
            ORDER BY no_unit ASC
        ";

        $asset = DB::select($query);
        return view('pages.report-maintenance.index', compact(['asset']));
    }

    public function downloadReport(Request $request)
    {
        $s_breakdown_from = $request->s_breakdown_from;
        $s_breakdown_to = $request->s_breakdown_to;
        $f_breakdown_from = $request->f_breakdown_from;
        $f_breakdown_to = $request->f_breakdown_to;
        $asset = $request->asset_id;

        $query = "
            SELECT A.*, B.*
            FROM maintenance A
            INNER JOIN asset B ON A.asset_id = B.asset_id 
            WHERE TRUE

        ";

        if ($s_breakdown_from && $s_breakdown_to != NULL) {
            $query .= " AND A.s_breakdown_date BETWEEN '$s_breakdown_from' AND '$s_breakdown_to'";
        }

        if ($f_breakdown_from && $f_breakdown_to != NULL) {
            $query .= " AND A.f_breakdown_date BETWEEN '$f_breakdown_from' AND '$f_breakdown_to'";
        }

        if ($asset != NULL) {
            $query .= " AND A.asset_id = $asset";
        }

        $query .= " ORDER BY A.s_breakdown_date, A.f_breakdown_date ASC";

        $data = DB::select($query);

        if ($asset != NULL) {
            $query_asset = "
                SELECT asset_id, no_unit, manufactur
                FROM asset
                WHERE asset_id = $asset
            ";
            $find_asset = DB::select($query_asset);
            $result_asset = $find_asset[0]->no_unit . ', ' . $find_asset[0]->manufactur;
        } else {
            $result_asset = '-';
        }

        $printOn = now()->format('d/m/Y H:i:s');

        $resultPrint = ''.$printOn . ' WIB';

        $req = (object) [
            's_breakdown_from' => $s_breakdown_to ? Carbon::parse($s_breakdown_to)->format('d/m/Y') : '',
            's_breakdown_to' => $s_breakdown_to ? Carbon::parse($s_breakdown_to)->format('d/m/Y') : '',
            'f_breakdown_from' => $f_breakdown_from ? Carbon::parse($f_breakdown_from)->format('d/m/Y') : '',
            'f_breakdown_to' => $f_breakdown_to ? Carbon::parse($f_breakdown_to)->format('d/m/Y') : '',
            'asset' => $result_asset,
            'print_on' => $resultPrint
        ];

        $timestamp = now()->format('Ymd');

        $pdf = PDF::loadView('pages.report-maintenance.downloadReport', compact(['data', 'req']));
        return $pdf->download('MaintenanceReport-' .$timestamp.'.pdf');
    }
}
