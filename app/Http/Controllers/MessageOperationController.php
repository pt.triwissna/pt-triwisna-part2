<?php

namespace App\Http\Controllers;

use App\Http\Controllers\Controller;
use App\Models\MessageOperationModel;
use App\Models\OperationsModels;
use Illuminate\Http\Request;
date_default_timezone_set("Asia/Jakarta");

class MessageOperationController extends Controller
{
    public function message(Request $request, $id) {
        $operation = OperationsModels::find($id);
        if($operation) {
            $validated = $request->validate([
                'message'   => 'required'
            ]);
            $data = [
                'date'          =>  now(),
                'message'       =>  $validated['message'],
                'by_role'          => 'S',
                'operation_id'  =>  $id
            ];
            MessageOperationModel::create($data);

            OperationsModels::where('operation_id', $id)->update([
                'flg'   => 'R'
            ]);

            return redirect()->route('index.operations')->with('success', 'Message Sent.');
        } else {
            return redirect()->route('index.operations')->with('error', 'Message Not Sent.');
        }
        
    }
}
