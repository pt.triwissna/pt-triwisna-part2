<?php

namespace App\Http\Controllers;

use App\Http\Controllers\Controller;
use App\Models\MaintenanceModel;
use App\Models\MessageMaintenanceModel;
use Illuminate\Http\Request;

class MessageMaintenanceController extends Controller
{
    public function message(Request $request, $id) {
        $maintenance = MaintenanceModel::find($id);
        if($maintenance) {
            $validated = $request->validate([
                'message'   => 'required'
            ]);
            $data = [
                'date'          =>  now(),
                'message'       =>  $validated['message'],
                'by_role'       => 'S',
                'maintenance_id'  =>  $id
            ];

            MessageMaintenanceModel::create($data);

            MaintenanceModel::where('maintenance_id', $id)->update([
                'flg'   => 'R'
            ]);
            return redirect()->route('maintenance')->with('success', 'Message Sent.');
        } else {
            return redirect()->route('maintenance')->with('error', 'Message Not Sent.');
        }
    }
}
