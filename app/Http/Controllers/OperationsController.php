<?php

namespace App\Http\Controllers;

use App\Models\assetModels;
use App\Models\MessageOperationModel;
use App\Models\OperationsModels;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use App\Rules\OdometerAndHourmeter;
use App\Rules\ValidTime;
use Illuminate\Support\Facades\DB;

class OperationsController extends Controller
{
    public function index(Request $request)
    {
        $no_unit = $request->no_unit;
        $date = $request->date;
        $shift = $request->shift;
        $query = OperationsModels::query()
            ->select(
                'A.*', 'B.manufactur',
                'B.model', 'B.yom', 'B.no_unit',
                'C.ctgr_name', 'D.type_name', 'E.username'
            )
            ->from('operation AS A')
            ->join('asset AS B', 'A.asset_id', '=', 'B.asset_id')
            ->join('ctgr_asset AS C', 'B.ctgr_asset_id', '=', 'C.ctgr_asset_id')
            ->join('type_asset AS D', 'B.type_asset_id', '=', 'D.type_asset_id')
            ->join('users AS E', 'A.record_id', '=', 'E.id')
            ->when($no_unit, function ($query, $no_unit) {
                return $query->where('A.no_unit', 'like', '%' . $no_unit . '%');
            })
            ->when($date, function ($query, $date) {
                return $query->where('A.date', $date);
            })
            ->when($shift, function ($query, $shift) {
                return $query->where('A.shift', $shift);
            })
            ->orderBy('A.date', 'desc')
        ;

        $operations = $query->paginate(20);

        $queryCount = "
            SELECT COUNT(1) AS totalData
            FROM operation
        ";
        $total = DB::select($queryCount);

        $query_message = "
            SELECT *
            FROM m_operation
            WHERE by_role = 'S'
            ORDER BY date DESC
        ";

        $messageOperation = DB::select($query_message); 

        return view('pages.operations.index', compact('operations', 'total', 'messageOperation'));
    }
    public function create()
    {
        $unit = assetModels::all();
        return view('pages.operations.create', compact([
            'unit',
        ]));
    }

    public function store(Request $request)
    {
        // Validasi data dari formulir
        $request->validate([
            'date' => 'required|date',
            'shift' => 'required',
            'project' => 'required',
            'operator' => 'required',
            'asset_id' => 'required',
            'fuel' => 'required|numeric',
            'trip' => 'required|numeric',
            's_worktime' => ['required', new ValidTime],
            'f_worktime' => ['required', new ValidTime],
            's_otherdelay' => ['nullable', new ValidTime],
            'f_otherdelay' => ['nullable', new ValidTime],
            's_odometer' => [ 'nullable', 'numeric', new OdometerAndHourmeter, ],
            'f_odometer' => 'nullable|numeric',
            's_hourmeter' => [ 'nullable', 'numeric', new OdometerAndHourmeter, ],
            'f_hourmeter' => 'nullable|numeric',
        ]);


        if (Auth::check()) {
            $user = Auth::user()->id;
            // Simpan data ke dalam database
            OperationsModels::create([
                'date' => $request->date,
                'shift' => $request->shift,
                'project' => $request->project,
                'operator' => $request->operator,
                'asset_id' => $request->asset_id,
                'fuel' => $request->fuel,
                'trip' => $request->trip,
                's_worktime' => $request->s_worktime,
                'f_worktime' => $request->f_worktime,
                's_odometer' => $request->s_odometer ?? NULL,
                'f_odometer' => $request->f_odometer ?? NULL,
                's_hourmeter' => $request->s_hourmeter ?? NULL,
                'f_hourmeter' => $request->f_hourmeter ?? NULL,
                's_otherdelay' => $request->s_otherdelay ?? NULL,
                'f_otherdelay' => $request->f_otherdelay ?? NULL,
                'flg'           => 'N',
                'record_id' => $user,

            ]);
            // Redirect atau berikan respon sesuai dengan kebutuhan Anda
            return redirect()->route('index.operations')->with('success', 'Data saved successfully');
        } else {
            return response()->json(['message' => 'Unauthorized'], 401);
        }
    }

    public function page_edit($id) {
       $operation = OperationsModels::find($id);
       $asset = assetModels::all();

        return view('pages.operations.edit', compact(['operation', 'asset']));
    }

    public function edit(Request $request, $id) {
        $request->validate([
            'date' => 'required|date',
            'shift' => 'required',
            'project' => 'required',
            'operator' => 'required',
            'asset_id' => 'required',
            'fuel' => 'required|numeric',
            'trip' => 'required|numeric',
            's_worktime' => ['required', new ValidTime],
            'f_worktime' => ['required', new ValidTime],
            's_otherdelay' => ['nullable', new ValidTime],
            'f_otherdelay' => ['nullable', new ValidTime],
            's_odometer' => [ 'nullable', 'numeric', new OdometerAndHourmeter, ],
            'f_odometer' => 'nullable|numeric',
            's_hourmeter' => [ 'nullable', 'numeric', new OdometerAndHourmeter, ],
            'f_hourmeter' => 'nullable|numeric',
        ]);

        $operation = OperationsModels::find($id);
        $operation->update([
            'date'          => $request->date,
            'shift'         => $request->shift,
            'project'       => $request->project,
            'operator'      => $request->operator,
            'asset_id'      => $request->asset_id,
            'fuel'          => $request->fuel,
            'trip'          => $request->trip,
            's_worktime'    => $request->s_worktime,
            'f_worktime'    => $request->f_worktime,
            's_otherdelay'  => $request->s_otherdelay,
            'f_otherdelay'  => $request->f_otherdelay,
            's_odometer'    => $request->s_odometer,
            'f_odometer'    => $request->f_odometer,
            's_hourmeter'   => $request->s_hourmeter,
            'f_hourmeter'   => $request->f_hourmeter,
            'flg'           => 'N',
        ]);

        return redirect()->route('index.operations')->with('success', 'Data edit was successfully');
    }

    public function revisi_page() {
        $query = "
            SELECT A.*, B.*, C.*
            FROM m_operation A
            INNER JOIN operation B ON A.operation_id = B.operation_id
            INNER JOIN asset C ON B.asset_id = C.asset_id
            ORDER BY A.date DESC
        ";

        $m_operation = DB::select($query);
        return view('pages.operations.revisionPage', compact(['m_operation']));
    }

    public function revisiOperation(Request $request, $id) {
        $m_revisi = MessageOperationModel::find($id);
        $operation_id = $m_revisi->operation_id;

        $operation = OperationsModels::find($operation_id);
        $asset = assetModels::all();
        $param = $id;
        return view('pages.operations.revisi', compact(['operation', 'asset', 'm_revisi', 'param']));
    }

    public function revisiAction(Request $request, $id, $param) {
        
        $request->validate([
            'date' => 'required|date',
            'shift' => 'required',
            'project' => 'required',
            'operator' => 'required',
            'asset_id' => 'required',
            'fuel' => 'required|numeric',
            'trip' => 'required|numeric',
            's_worktime' => ['required', new ValidTime],
            'f_worktime' => ['required', new ValidTime],
            's_otherdelay' => ['nullable', new ValidTime],
            'f_otherdelay' => ['nullable', new ValidTime],
            's_odometer' => [ 'nullable', 'numeric', new OdometerAndHourmeter, ],
            'f_odometer' => 'nullable|numeric',
            's_hourmeter' => [ 'nullable', 'numeric', new OdometerAndHourmeter, ],
            'f_hourmeter' => 'nullable|numeric',
        ]);

        $operation = OperationsModels::find($id);
        $operation->update([
            'date'          => $request->date,
            'shift'         => $request->shift,
            'project'       => $request->project,
            'operator'      => $request->operator,
            'asset_id'      => $request->asset_id,
            'fuel'          => $request->fuel,
            'trip'          => $request->trip,
            's_worktime'    => $request->s_worktime,
            'f_worktime'    => $request->f_worktime,
            's_otherdelay'  => $request->s_otherdelay,
            'f_otherdelay'  => $request->f_otherdelay,
            's_odometer'    => $request->s_odometer,
            'f_odometer'    => $request->f_odometer,
            's_hourmeter'   => $request->s_hourmeter,
            'f_hourmeter'   => $request->f_hourmeter,
            'flg'           => 'N'
        ]);

        $revisi = MessageOperationModel::findOrFail($param);
        $revisi->delete();

        return redirect()->route('revisiPage.operation')->with('success', 'Data revisi was successfully');
    }

    public function delete($id) {
        OperationsModels::find($id)->delete();
        return redirect()->route('index.operations')->with('success', 'Data deleted successfully');
    }
}
