<?php

namespace App\Http\Controllers;

use App\Models\assetModels;
use App\Models\CtgrAssetModels;
use App\Models\permitsModels;
use App\Models\TypeAssetModels;
use App\Models\typeInsuranceModels;
use Database\Factories\PermitsInsuranceFactory;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\DB;

class assetController extends Controller
{
    public function index(Request $request)
    {

        $ctgr_asset = CtgrAssetModels::all();
        $type_asset = TypeAssetModels::all();

        $no_unit = $request->no_unit;
        $type_asset_id = $request->type_asset;
        $ctgr_asset_id = $request->ctgr_asset;

        $query = assetModels::query()
            ->select(
                'A.asset_id',
                'A.record_id',
                'A.no_unit',
                'A.type_asset_id',
                'A.ctgr_asset_id',
                'A.manufactur',
                'A.model',
                'A.yom',
                'A.permit_insurance_id',
                'B.type_name',
                'C.ctgr_name',
                'D.insurance_issued',
                'D.insurance_expired',
                'D.stnk_issued',
                'D.stnk_expired',
                'D.kir_issued',
                'D.kir_expired',
                'F.insurance_type_id',
                'F.insurance_type_name',
                'E.username'
            )
            ->from('asset AS A')
            ->join('type_asset AS B', 'A.type_asset_id', '=', 'B.type_asset_id')
            ->join('ctgr_asset AS C', 'A.ctgr_asset_id', '=', 'C.ctgr_asset_id')
            ->leftJoin('permit_insurance AS D', 'A.permit_insurance_id', '=', 'D.permit_insurance_id')
            ->join('users AS E', 'A.record_id', '=', 'E.id')
            ->leftJoin('insurance_type AS F', 'D.insurance_type_id', '=', 'F.insurance_type_id')
            ->when($no_unit, function ($query, $no_unit) {
                return $query->where('A.no_unit', 'like', '%' . $no_unit . '%');
            })
            ->when($type_asset_id, function ($query, $type_asset_id) {
                return $query->where('A.type_asset_id', $type_asset_id);
            })
            ->when($ctgr_asset_id, function ($query, $ctgr_asset_id) {
                return $query->where('A.ctgr_asset_id', $ctgr_asset_id);
            })
            ->orderBy('A.manufactur', 'asc');

        $asset = $query->paginate(20);


        $queryCount = "
            SELECT COUNT(1) AS totalData
            FROM asset
        ";
        $total = DB::select($queryCount);

        return view('pages.asset.index', compact(['ctgr_asset', 'type_asset', 'asset', 'total']));
    }
    public function create()
    {
        $category = CtgrAssetModels::all();
        $type = TypeAssetModels::all();
        $insurance = typeInsuranceModels::all();
        return view('pages.asset.create', compact([
            'category',
            'type',
            'insurance',
        ]));
    }


    public function store(Request $request)
    {
        $request->validate([
            'id_type' => 'required',
            'id_ctgr' => 'required',
            'manufacture' => 'required',
            'model' => 'required',
            'yom' => 'required|numeric',
            'no_unit' => 'required',
            'id_insurance_type' => 'nullable',
            'insurance_issued' => 'nullable|numeric',
            'insurance_expired' => 'nullable|numeric',
            'stnk_issued' => 'nullable|numeric',
            'stnk_expired' => 'nullable|numeric',
            'kir_issued' => 'nullable|numeric',
            'kir_expired' => 'nullable|numeric',
        ]);

        if (Auth::check()) {
            $user = Auth::user()->id;
            if ($request->id_insurance_type !== null) {
                $newPermits = permitsModels::create([
                    'insurance_type_id' => $request->id_insurance_type,
                    'insurance_issued' => $request->insurance_issued,
                    'insurance_expired' => $request->insurance_expired,
                    'stnk_issued' => $request->stnk_issued,
                    'stnk_expired' => $request->stnk_expired,
                    'kir_issued' => $request->kir_issued,
                    'kir_expired' => $request->kir_expired,
                ]);
                $id_permits = $newPermits->permit_insurance_id;

                // Simpan data ke dalam database
                assetModels::create([
                    'type_asset_id' => $request->id_type,
                    'ctgr_asset_id' => $request->id_ctgr,
                    'permit_insurance_id' => $id_permits,
                    'manufactur' => $request->manufacture,
                    'model' => $request->model,
                    'yom' => $request->yom,
                    'no_unit' => $request->no_unit,
                    'record_id' => $user,
                ]);
                return redirect()->route('index.asset')->with('success', 'Data saved successfully');
            } else {
                assetModels::create([
                    'type_asset_id' => $request->id_type,
                    'ctgr_asset_id' => $request->id_ctgr,
                    'manufactur' => $request->manufacture,
                    'model' => $request->model,
                    'yom' => $request->yom,
                    'no_unit' => $request->no_unit,
                    'record_id' => $user,
                ]);
                return redirect()->route('index.asset')->with('success', 'Data saved successfully');
            }
        } else {
            return response()->json(['message' => 'Unauthorized'], 401);
        }
    }

    public function edit_page($id)
    {

        $asset = assetModels::find($id);

        $category = CtgrAssetModels::all();
        $type = TypeAssetModels::all();
        $insurance = typeInsuranceModels::all();

        $permit = permitsModels::find($asset->permit_insurance_id);
        return view('pages.asset.edit', compact(['asset', 'category', 'type', 'insurance', 'permit']));
    }

    public function edit(Request $request, $id)
    {
        $request->validate([
            'type_asset_id' => 'required',
            'ctgr_asset_id' => 'required',
            'manufactur' => 'required',
            'model' => 'required',
            'yom' => 'required|numeric',
            'no_unit' => 'required',
            'insurance_issued' => 'nullable|numeric',
            'insurance_expired' => 'nullable|numeric',
            'stnk_issued' => 'nullable|numeric',
            'stnk_expired' => 'nullable|numeric',
            'kir_issued' => 'nullable|numeric',
            'kir_expired' => 'nullable|numeric',
            'insurance_type_id' => 'nullable'
        ]);

        $asset = assetModels::find($id);

        $assetData = [
            'type_asset_id' => $request->type_asset_id,
            'ctgr_asset_id' => $request->ctgr_asset_id,
            'manufactur' => $request->manufactur,
            'model' => $request->model,
            'yom' => $request->yom,
            'no_unit' => $request->no_unit,
        ];

        $asset->update($assetData);

        // Check if insurance_type_id is present
        if ($request->filled('insurance_type_id')) {
            $permitData = [
                'insurance_type_id' => $request->insurance_type_id,
                'insurance_issued' => $request->insurance_issued,
                'insurance_expired' => $request->insurance_expired,
                'stnk_issued' => $request->stnk_issued,
                'stnk_expired' => $request->stnk_expired,
                'kir_issued' => $request->kir_issued,
                'kir_expired' => $request->kir_expired,
            ];

            $permit = permitsModels::updateOrCreate(['permit_insurance_id' => $asset->permit_insurance_id], $permitData);
            $asset->update(['permit_insurance_id' => $permit->permit_insurance_id]);
        }
        return redirect()->route('index.asset')->with('success', 'Data edit was successfully');
    }



    public function confirmDelete($id)
    {
        $asset = assetModels::find($id);

        if($asset->permit_insurance_id  != NULL || $asset->permit_insurance_id  != 0) {
            DB::table('permit_insurance')->where('permit_insurance_id', '=', $asset->permit_insurance_id)->delete();
        }

        DB::table('asset')->where('asset_id', '=', $id)->delete();
        return redirect()->route('index.asset')->with('success', 'Data deleted successfully');
    }
}
