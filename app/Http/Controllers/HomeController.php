<?php

namespace App\Http\Controllers;

use App\Models\assetModels;
use App\Models\MaintenanceModel;
use App\Models\OperationsModels;
use Illuminate\Support\Facades\DB;

class HomeController extends Controller
{
  public function index()
  {
    // operation
    $total_data_operation = OperationsModels::select(DB::raw("COUNT(operation_id) as total_data_operation"))
      ->groupBy(DB::raw('YEAR(date)'), DB::raw('MONTH(date)'))
      ->pluck('total_data_operation');

    $bulan_operation = OperationsModels::select(
      DB::raw("GROUP_CONCAT(DISTINCT DATE_FORMAT(date, '%M %Y')) as bulan_operation")
    )
      ->groupBy(DB::raw('YEAR(date)'), DB::raw('MONTH(date)'))
      ->pluck('bulan_operation');

    // maintenannce
    $total_finance = MaintenanceModel::select(DB::raw("CAST(SUM(finance) as int ) as total_finance"))
      ->groupBy(DB::raw("YEAR(s_breakdown_date)"), DB::raw('MONTH(s_breakdown_date)'))
      ->pluck('total_finance');

    $bulan_maintenance = MaintenanceModel::select(
      DB::raw("GROUP_CONCAT(DISTINCT DATE_FORMAT(s_breakdown_date, '%M %Y')) as bulan_maintenance")
    )
      ->groupBy(DB::raw('YEAR(s_breakdown_date)'), DB::raw('MONTH(s_breakdown_date)'))
      ->pluck('bulan_maintenance');


    // total data keseluruhan
    $total_operation = OperationsModels::count();
    $total_maintenance = MaintenanceModel::count();
    $total_asset = assetModels::count();


    // dd($total_operation);

    return view('pages.home', compact(['total_data_operation', 'bulan_operation', 'total_finance', 'bulan_maintenance', 'total_operation', 'total_maintenance', 'total_asset']));
  }


  public function getRealtimeData()
  {
    $totalPeminjaman = DB::table('operation_active')->count();
    $totalPerbaikan = DB::table('maintenance_active')->count();

    return response()->json([
      'total_peminjaman' => $totalPeminjaman,
      'total_perbaikan' => $totalPerbaikan,
    ]);
  }
}
