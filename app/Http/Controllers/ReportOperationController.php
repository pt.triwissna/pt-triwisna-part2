<?php

namespace App\Http\Controllers;

use App\Http\Controllers\Controller;
use \Barryvdh\DomPDF\Facade\PDF;
use Carbon\Carbon;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;

date_default_timezone_set("Asia/Jakarta");

class ReportOperationController extends Controller
{
    public function index()
    {
        $query = "
            SELECT asset_id, no_unit, manufactur, model 
            FROM asset
            ORDER BY no_unit ASC
        ";

        $asset = DB::select($query);
        return view('pages.report-operation.index', compact(['asset']));
    }

    public function downloadReport(Request $request)
    {

        $date_to = $request->date_to;
        $date_from = $request->date_from;
        $shift = $request->shift;
        $asset = $request->asset_id;

        $query = "
            SELECT A.*, B.*
            FROM operation A
            INNER JOIN asset B ON A.asset_id = B.asset_id
            WHERE TRUE
        ";

        if ($date_from && $date_to != NULL) {
            $query .= " AND A.date BETWEEN '$date_from' AND '$date_to'";
        }

        if ($shift != NULL) {
            $query .= " AND A.shift = '$shift'";
        }

        if ($asset != NULL) {
            $query .= " AND A.asset_id = $asset";
        }

        $query .= " ORDER BY A.date ASC";

        $data = DB::select($query);


        $printOn = now()->format('d/m/Y H:i:s');

        $resultPrint = '' . $printOn . ' WIB';

        if ($asset != NULL) {
            $query_asset = "
                SELECT asset_id, no_unit, manufactur
                FROM asset
                WHERE asset_id = $asset
            ";
            $find_asset = DB::select($query_asset);
            $result_asset = $find_asset[0]->no_unit . ', ' . $find_asset[0]->manufactur;
        } else {
            $result_asset = '-';
        }

        $req = (object) [
            'date_to' => $date_to ?  Carbon::parse($date_to)->format('d/m/Y') : '',
            'date_from' => $date_from ?  Carbon::parse($date_from)->format('d/m/Y') : '',
            'shift' => $request->shift ?? '-',
            'asset' => $result_asset,
            'print_on' => $resultPrint
        ];

        $timestamp = now()->format('Ymd');

        $pdf = PDF::loadView('pages.report-operation.downloadReport', compact(['data', 'req']));
        $pdf->setPaper('A4', 'landscape');

        return $pdf->download('OperationReport-' . $timestamp . '.pdf');
    }
}
