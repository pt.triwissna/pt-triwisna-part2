<?php

namespace App\Http\Controllers;

use App\Http\Controllers\Controller;
use App\Models\assetModels;
use App\Models\MaintenanceModel;
use App\Models\MessageMaintenanceModel;
use Carbon\Carbon;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\DB;

class MaintenanceController extends Controller
{
    public function index(Request $request)
    {
        $s_breakdown = $request->s_breakdown_date;
        $f_breakdown = $request->f_breakdown_date;
        $no_unit     = $request->no_unit;

        $query = MaintenanceModel::query()
            ->select('A.maintenance_id', 'A.record_id', 'A.asset_id', 'A.s_breakdown_date', 'A.s_breakdown_time', 'A.f_breakdown_date', 'A.f_breakdown_time', 'A.issue', 'A.perform_by', 'A.finance', 'B.no_unit', 'B.type_asset_id', 'B.ctgr_asset_id', 'B.manufactur', 'B.model', 'B.yom', 'C.type_name', 'D.ctgr_name', 'E.username', 'flg')
            ->from('maintenance AS A')
            ->join('asset AS B', 'A.asset_id', '=', 'B.asset_id')
            ->join('type_asset AS C', 'B.type_asset_id', '=', 'C.type_asset_id')
            ->join('ctgr_asset AS D', 'B.ctgr_asset_id', '=', 'D.ctgr_asset_id')
            ->join('users AS E', 'A.record_id', '=', 'E.id')
            ->when($s_breakdown, function ($query, $s_breakdown) {
                return $query->where('A.s_breakdown_date', $s_breakdown);
            })
            ->when($f_breakdown, function ($query, $f_breakdown) {
                return $query->where('A.f_breakdown_date', $f_breakdown);
            })
            ->when($no_unit, function ($query, $no_unit) {
                return $query->where('B.no_unit', 'like', '%' . $no_unit . '%');
            })
            ->orderBy('A.f_breakdown_date', 'desc');

        $maintenance = $query->paginate(20);


        // Count
        $queryCount = "
            SELECT COUNT(1) AS totalData
            FROM maintenance
        ";
        $total = DB::select($queryCount);

        foreach ($maintenance as $record) {
            $record->s_breakdown_date = Carbon::createFromFormat('Y-m-d', $record->s_breakdown_date)->format('d/m/Y');
            $record->f_breakdown_date = Carbon::createFromFormat('Y-m-d', $record->f_breakdown_date)->format('d/m/Y');
            $record->s_breakdown_time = Carbon::createFromFormat('H:i:s', $record->s_breakdown_time)->format('H:i');
            $record->f_breakdown_time = Carbon::createFromFormat('H:i:s', $record->f_breakdown_time)->format('H:i');
        }

        $query_message = "
            SELECT *
            FROM m_maintenance
            WHERE by_role = 'S'
            ORDER BY date DESC
        ";

        $messageMaintenance = DB::select($query_message); 

        return view('pages.maintenance.index', compact(['maintenance', 'total', 'messageMaintenance']));
    }

    public function page_add()
    {
        $query = "
            SELECT asset_id, no_unit, manufactur, model 
            FROM asset
            ORDER BY no_unit ASC
        ";

        $asset = DB::select($query);
        return view('pages.maintenance.create', compact([
            'asset'
        ]));
    }

    public function add_maintenance(Request $request)
    {
        $validatedData = $request->validate([
            's_breakdown'   => 'required',
            'f_breakdown'   => 'required',
            'asset_id'      => 'required',
            'issue'         => 'required',
            'perform_by'    => 'required',
            'finance'       => 'required'
        ]);

        $s_breakdown = $validatedData['s_breakdown'];
        $f_breakdown = $validatedData['f_breakdown'];

        $s_breakdown_date = date('Y-m-d', strtotime($s_breakdown));
        $s_breakdown_time = date('H:i:s', strtotime($s_breakdown));

        $f_breakdown_date = date('Y-m-d', strtotime($f_breakdown));
        $f_breakdown_time = date('H:i:s', strtotime($f_breakdown));

        $user_id = Auth::user()->id;

        $data = [
            'record_id'         => $user_id,
            's_breakdown_date'  => $s_breakdown_date,
            's_breakdown_time'  => $s_breakdown_time,
            'f_breakdown_date'  => $f_breakdown_date,
            'f_breakdown_time'  => $f_breakdown_time,
            'asset_id'          => $validatedData['asset_id'],
            'issue'             => $validatedData['issue'],
            'perform_by'        => $validatedData['perform_by'],
            'finance'           => $validatedData['finance'],
            'flg'               => 'N'
        ];

        MaintenanceModel::create($data);

        return redirect()->route('maintenance')->with('success', 'Data saved Successfully');
    }

    public function page_edit($id) {
        $maintenance = MaintenanceModel::find($id);
        $asset = assetModels::all();

        return view('pages.maintenance.edit', compact(['maintenance', 'asset']));
    }

    public function edit(Request $request, $id) {
        $request->validate([
            's_breakdown_date'  => 'required',
            's_breakdown_time'  => 'required',
            'f_breakdown_date'  => 'required',
            'f_breakdown_time'  => 'required',
            'asset_id'          => 'required',
            'issue'             => 'required',
            'perform_by'        => 'required',
            'finance'           => 'required'
        ]);

        $maintenance = MaintenanceModel::find($id);
        $maintenance->update([
            's_breakdown_date'  => $request->s_breakdown_date,
            's_breakdown_time'  => $request->s_breakdown_time,
            'f_breakdown_date'  => $request->f_breakdown_date,
            'f_breakdown_time'  => $request->f_breakdown_time,
            'asset_id'          => $request->asset_id,
            'issue'             => $request->issue,
            'perform_by'        => $request->perform_by,
            'finance'           => $request->finance
        ]);

        return redirect()->route('maintenance')->with('success', 'Data edit was successfully');
    }

    public function revisi_page() {
        $query = "
            SELECT A.*, B.*, C.*
            FROM m_maintenance A
            INNER JOIN maintenance B ON A.maintenance_id = B.maintenance_id
            INNER JOIN asset C ON B.asset_id = C.asset_id
            ORDER BY A.date DESC
        ";

        $m_maintenance = DB::select($query);
        return view('pages.maintenance.revisionPage', compact(['m_maintenance']));
    }

    public function revisiMaintenance($id) {
        $m_revisi = MessageMaintenanceModel::find($id);
        $maintenance_id = $m_revisi->maintenance_id;

        $maintenance = MaintenanceModel::find($maintenance_id);
        $asset = assetModels::all();
        $param = $id;

        return view('pages.maintenance.revisi', compact(['m_revisi', 'maintenance', 'asset', 'param']));
    }

    public function revisiAction(Request $request, $id, $param) {
        $request->validate([
            's_breakdown_date'  => 'required',
            's_breakdown_time'  => 'required',
            'f_breakdown_date'  => 'required',
            'f_breakdown_time'  => 'required',
            'asset_id'          => 'required',
            'issue'             => 'required',
            'perform_by'        => 'required',
            'finance'           => 'required'
        ]);
        $maintenance = MaintenanceModel::find($id);
        $maintenance->update([
            's_breakdown_date'  => $request->s_breakdown_date,
            's_breakdown_time'  => $request->s_breakdown_time,
            'f_breakdown_date'  => $request->f_breakdown_date,
            'f_breakdown_time'  => $request->f_breakdown_time,
            'asset_id'          => $request->asset_id,
            'issue'             => $request->issue,
            'perform_by'        => $request->perform_by,
            'finance'           => $request->finance,
            'flg'               => 'N'
        ]);

        $revisi = MessageMaintenanceModel::findOrFail($param);
        $revisi->delete();

        return redirect()->route('revisiPage.maintenance')->with('success', 'Data revisi was successfully');
    }

    public function delete($id) {
        // DB::table('maintenance')->where('maintenance_id  ', '=', $id)->delete();
        $maintenance = MaintenanceModel::find($id)->delete();
        return redirect()->route('maintenance')->with('success', 'Data deleted successfully');
    }
}
